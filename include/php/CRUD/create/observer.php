<?php
require_once "../../../db/DBconnect.php";

$vision = $_POST['vision'];
$age = $_POST['age'];
$gender = $_POST['gender'];
$tasks = json_decode( $_POST['tasks'], TRUE );


// Create observer:
$sql = "INSERT INTO observers (vision, age, gender) VALUES (?,?,?)";
$sth = $db->prepare($sql);
if( $sth->execute(array($vision, $age, $gender )) )	// Execute the query.
  echo "added observer";




// Get last inserted ID from observers:
$curID = $db->lastInsertId();
/*----------*/

// Add observer's tasks:

foreach($tasks as $task=>$val)
{
  $sql = "INSERT INTO tasks (observerID, taskID, taskAnswer, taskCorrect, taskTime) VALUES (?,?,?,?,?)";
  $sth = $db->prepare($sql);
  if( $sth->execute(array($curID, $task+1, $val['answer'], $val['correct'], $val['time'])) )	// Execute the query.
    echo "added task";

}






