<?php

// Create database
$sql = "CREATE DATABASE IF NOT EXISTS MasterExperiment; 
USE MasterExperiment;

CREATE TABLE IF NOT EXISTS observers 
( 
  ID INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
  vision VARCHAR(64),
  age INT,
  gender INT,
  group INT
)";
$sth = $db->prepare($sql);
$sth->execute();
										
// Create Table tasks:
$sql ="	CREATE TABLE IF NOT EXISTS tasks
(
  observerID INT NOT NULL,
  taskID INT NOT NULL,
  taskAnswer INT NOT NULL,
  taskCorrect INT NOT NULL,
  taskTime INT NOT NULL,
  
  FOREIGN KEY (observerID) REFERENCES observers(ID),
  PRIMARY KEY (taskID, observerID)
)";

$sth = $db->prepare($sql);
$sth->execute();	