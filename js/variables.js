'use strict';

let start;
let stop;
let time;

let lastQuestion = false;
let daltIndex = 0; 					// Index to use depending on CVD type.
let taskIndex = 0; 					// The index counter for experiment stimuli.

const PAUSE = 1000;					// In milliseconds
const STROKE_WIDTH = 3;
const STROKE_WIDTH_GHOST = 40;

let observer; 							// Define observer object.

let legend =
[
	// [0] Original:
	[
		{name: "Road 1", color: "#ff0000"},
		{name: "Road 2", color: "#ff6666"},
		{name: "Road 3", color: "#d2733c"},
		{name: "Road 4", color: "#995f20"},
		{name: "Road 5", color: "#9bb93c"},
		{name: "Road 6", color: "#b487ff"},
		{name: "Road 7", color: "#be5abe"},
		{name: "Road 8", color: "#c31e28"}
	],
	/*----------------------------------------*/
	// [1] Daltonized Protanopia:
	[
		{name: "Road 1", color: "#fe819c"},
		{name: "Road 2", color: "#ffb3c4"},
		{name: "Road 3", color: "#d2a376"},
		{name: "Road 4", color: "#997c44"},
		{name: "Road 5", color: "#9ba92a"},
		{name: "Road 6", color: "#b39dff"},
		{name: "Road 7", color: "#be8cf9"},
		{name: "Road 8", color: "#c3728e"}
	],
	// [2] Daltonized Protanomaly:
	[
		{name: "Road 1", color: "#ff414e"},
		{name: "Road 2", color: "#ff8d95"},
		{name: "Road 3", color: "#d28b59"},
		{name: "Road 4", color: "#996e32"},
		{name: "Road 5", color: "#9bb133"},
		{name: "Road 6", color: "#b492ff"},
		{name: "Road 7", color: "#be73dc"},
		{name: "Road 8", color: "#c3485b"}
	],
	// [3] Daltonized Deuteraniopia:
	[
		{name: "Road 1", color: "#c3485b"},
		{name: "Road 2", color: "#ff85b5"},
		{name: "Road 3", color: "#d2866d"},
		{name: "Road 4", color: "#996a3e"},
		{name: "Road 5", color: "#9bb22c"},
		{name: "Road 6", color: "#b390ff"},
		{name: "Road 7", color: "#be6df2"},
		{name: "Road 8", color: "#c33f7d"}
	],
	// [4] Daltonized Deuteranomaly:
	[
		{name: "Road 1", color: "#ff1a42"},
		{name: "Road 2", color: "#ff768e"},
		{name: "Road 3", color: "#d27d54"},
		{name: "Road 4", color: "#99652f"},
		{name: "Road 5", color: "#9bb634"},
		{name: "Road 6", color: "#b48bff"},
		{name: "Road 7", color: "#be64d8"},
		{name: "Road 8", color: "#c32f53"}
	]
];

let tasks = 
[
	
	// Original palette:
	{
		// 0
		img: "img/14/0.png",
		corRoad: 1,
		objectiveRoad:"Road 4",
		x: "0",
		y: "0",
		
		paths : function(legendIndex)
		{
		 return	`<path onclick="nextTask($(this))" pathID="0" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][2].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 807.8271,815.71262 29.78972,-30.66589 21.90421,-28.91355 21.02804,-34.17056 23.65654,-28.91355 28.91355,-28.91356 35.9229,-24.53271 42.93224,-24.53271 49.9416,-18.39953 63.0841,-14.89486 78.8551,-16.6472 73.5982,-17.52336 45.5607,-8.76168 30.6659,-2.62851 0,0"/> 
		 <path onclick="nextTask($(this))" pathID="0" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][2].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 807.8271,815.71262 29.78972,-30.66589 21.90421,-28.91355 21.02804,-34.17056 23.65654,-28.91355 28.91355,-28.91356 35.9229,-24.53271 42.93224,-24.53271 49.9416,-18.39953 63.0841,-14.89486 78.8551,-16.6472 73.5982,-17.52336 45.5607,-8.76168 30.6659,-2.62851 0,0"/> 
		 <path onclick="nextTask($(this))" pathID="1" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][3].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 746.55111,556.8777 -19.20587,0.61954 -12.39089,0.61955 -8.05408,-3.71727 -14.24953,-9.29317 -22.3036,-14.86906 -19.82542,-12.39089 -21.68406,-9.91272 -22.92315,-9.29316 -25.40132,-9.29317 -18.58633,-8.05408 -16.10816,-4.95635 -9.91271,-3.71727 -10.53226,2.47818 -13.01043,3.71726 -15.48862,4.33681 -9.29316,0.61955 -6.81499,-1.85863 -6.19545,-3.71727 -8.67362,-14.86907 -8.67362,-17.34724 -10.53226,-22.30361 -5.5759,-11.1518 -4.33681,-8.67362 0.61954,-3.71727 1.85864,-6.81499 5.5759,-16.7277 9.91271,-19.82542 6.19544,-8.05408 8.67363,-6.19545 16.7277,-3.09772 10.53225,-1.23909 29.11859,-6.81499 22.92315,-7.43453 19.20588,-6.81499 9.29317,-8.67362 9.29317,-9.91271 5.5759,-5.5759" /> 
		 <path onclick="nextTask($(this))" pathID="1" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][3].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 746.55111,556.8777 -19.20587,0.61954 -12.39089,0.61955 -8.05408,-3.71727 -14.24953,-9.29317 -22.3036,-14.86906 -19.82542,-12.39089 -21.68406,-9.91272 -22.92315,-9.29316 -25.40132,-9.29317 -18.58633,-8.05408 -16.10816,-4.95635 -9.91271,-3.71727 -10.53226,2.47818 -13.01043,3.71726 -15.48862,4.33681 -9.29316,0.61955 -6.81499,-1.85863 -6.19545,-3.71727 -8.67362,-14.86907 -8.67362,-17.34724 -10.53226,-22.30361 -5.5759,-11.1518 -4.33681,-8.67362 0.61954,-3.71727 1.85864,-6.81499 5.5759,-16.7277 9.91271,-19.82542 6.19544,-8.05408 8.67363,-6.19545 16.7277,-3.09772 10.53225,-1.23909 29.11859,-6.81499 22.92315,-7.43453 19.20588,-6.81499 9.29317,-8.67362 9.29317,-9.91271 5.5759,-5.5759" /> 
		 <path onclick="nextTask($(this))" pathID="2" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][7].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 1162.2655,977.54841 13.0104,-21.06451 9.9127,-7.43454 13.63,-9.91271 10.5323,-4.95635 13.0104,-4.95636 14.8691,-2.47818 11.1518,-3.09772 14.2495,-6.81499 23.5427,-13.01043 16.1081,-11.15181 16.1082,-8.67362 16.7277,-10.53225 26.0209,-9.91272 16.1081,-8.67362 9.9127,-4.33681 7.4346,-0.61955 11.7713,14.24953 25.4013,25.40132 15.4886,16.7277 6.1955,6.19545 2.4782,6.81499 -10.5323,27.25996 -12.3909,23.54269 -10.5322,12.39089 -9.2932,9.91271 -5.5759,6.19544"/>
		 <path onclick="nextTask($(this))" pathID="2" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][7].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 1162.2655,977.54841 13.0104,-21.06451 9.9127,-7.43454 13.63,-9.91271 10.5323,-4.95635 13.0104,-4.95636 14.8691,-2.47818 11.1518,-3.09772 14.2495,-6.81499 23.5427,-13.01043 16.1081,-11.15181 16.1082,-8.67362 16.7277,-10.53225 26.0209,-9.91272 16.1081,-8.67362 9.9127,-4.33681 7.4346,-0.61955 11.7713,14.24953 25.4013,25.40132 15.4886,16.7277 6.1955,6.19545 2.4782,6.81499 -10.5323,27.25996 -12.3909,23.54269 -10.5322,12.39089 -9.2932,9.91271 -5.5759,6.19544"/>`;
		}
		
	},
	{
		// 1
		img: "img/14/1.png",
		corRoad: 1,
		objectiveRoad:"Road 3",
		x: "0",
		y: "0",
		paths: function(legendIndex)
		{
			return `<path onclick="nextTask($(this))" pathID="0" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][0].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 1533.9548,971.4044 -80.5502,73.5458 -4.5028,-14.0087 -5.0031,-11.5072 -7.5047,-8.5053 -29.5183,-30.01868 -18.5116,-21.51339 -13.0081,-17.5109 -19.5121,-24.51526 -19.0118,-22.0137 -9.5059,-14.00872 -9.506,-9.00561 -16.0099,-10.50654 -26.0162,-15.00934 -20.5128,-11.00685 -25.5159,29.51837 -29.5183,36.02242 -133.0829,154.0959"/>
			<path onclick="nextTask($(this))" pathID="0" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][0].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 1533.9548,971.4044 -80.5502,73.5458 -4.5028,-14.0087 -5.0031,-11.5072 -7.5047,-8.5053 -29.5183,-30.01868 -18.5116,-21.51339 -13.0081,-17.5109 -19.5121,-24.51526 -19.0118,-22.0137 -9.5059,-14.00872 -9.506,-9.00561 -16.0099,-10.50654 -26.0162,-15.00934 -20.5128,-11.00685 -25.5159,29.51837 -29.5183,36.02242 -133.0829,154.0959"/>
		 <path onclick="nextTask($(this))" pathID="1" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][2].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 596.8715,1061.4605 -37.52335,-17.0106 -16.00997,-10.5066 -11.00685,-20.5127 -3.50218,-17.01063 -3.50218,-28.01744 -4.00249,-21.51339 3.50218,-13.50841 2.50156,-19.01183 -2.00125,-30.519 1.00063,-46.52896 4.00249,-12.00747 9.50591,-15.50965 -0.50031,-18.51153 -7.50467,-23.51463 -1.00062,-13.0081 6.00374,-24.01495 4.5028,-33.52086 2.50155,-28.51775 1.50094,-10.00623 4.00249,-7.00436 2.50156,-4.5028 11.00685,-7.50467 18.01121,-4.00249 18.01121,-6.00374 14.00872,-12.50778 17.01059,-19.51215 5.50342,-6.00374 -9.50592,-13.00809 -10.00622,-24.51526"/>
		 <path onclick="nextTask($(this))" pathID="1" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][2].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 596.8715,1061.4605 -37.52335,-17.0106 -16.00997,-10.5066 -11.00685,-20.5127 -3.50218,-17.01063 -3.50218,-28.01744 -4.00249,-21.51339 3.50218,-13.50841 2.50156,-19.01183 -2.00125,-30.519 1.00063,-46.52896 4.00249,-12.00747 9.50591,-15.50965 -0.50031,-18.51153 -7.50467,-23.51463 -1.00062,-13.0081 6.00374,-24.01495 4.5028,-33.52086 2.50155,-28.51775 1.50094,-10.00623 4.00249,-7.00436 2.50156,-4.5028 11.00685,-7.50467 18.01121,-4.00249 18.01121,-6.00374 14.00872,-12.50778 17.01059,-19.51215 5.50342,-6.00374 -9.50592,-13.00809 -10.00622,-24.51526"/>
		 <path onclick="nextTask($(this))" pathID="2" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][4].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none" d="M 950.9434,307.78302 974.29245,300 1012.5,290.09434 l 19.8113,-16.27359 28.3019,-31.83962 19.1038,-19.81132 12.7358,-16.27359 16.9812,-7.07547 16.9811,-4.95283 13.4434,-1.41509 12.0283,2.12264 14.1509,2.12264 4.2453,1.4151 1.4151,-11.32076 -1.4151,-6.36792 -4.9528,-7.07547 -4.9528,-4.24529 -9.9057,-4.95283 -14.1509,-0.70755 -12.7359,2.83019 -15.566,2.12264 -18.3963,-9.90566 -22.6415,-16.27358 -22.6415,-17.68868 -11.3207,-6.36792 -8.4906,-2.83019 -9.9057,-0.70755 -12.7358,0 -14.15095,-2.83019 -16.27358,-9.90566 L 956.60378,92.688678 949.5283,87.0283 940.33019,84.198112 928.30189,82.783017 916.98113,82.07547 907.07547,80.660376 900,77.12264 l -6.36792,-10.613208 -5.66038,-9.90566 -4.95283,-8.490566 -3.53774,-3.537736 -4.95283,0 -4.95283,0 -2.12264,0.707547"/>
		 <path onclick="nextTask($(this))" pathID="2" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][4].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none" d="M 950.9434,307.78302 974.29245,300 1012.5,290.09434 l 19.8113,-16.27359 28.3019,-31.83962 19.1038,-19.81132 12.7358,-16.27359 16.9812,-7.07547 16.9811,-4.95283 13.4434,-1.41509 12.0283,2.12264 14.1509,2.12264 4.2453,1.4151 1.4151,-11.32076 -1.4151,-6.36792 -4.9528,-7.07547 -4.9528,-4.24529 -9.9057,-4.95283 -14.1509,-0.70755 -12.7359,2.83019 -15.566,2.12264 -18.3963,-9.90566 -22.6415,-16.27358 -22.6415,-17.68868 -11.3207,-6.36792 -8.4906,-2.83019 -9.9057,-0.70755 -12.7358,0 -14.15095,-2.83019 -16.27358,-9.90566 L 956.60378,92.688678 949.5283,87.0283 940.33019,84.198112 928.30189,82.783017 916.98113,82.07547 907.07547,80.660376 900,77.12264 l -6.36792,-10.613208 -5.66038,-9.90566 -4.95283,-8.490566 -3.53774,-3.537736 -4.95283,0 -4.95283,0 -2.12264,0.707547"/>`;
		}
	},
	{
		// 2
		img: "img/14/2.png",
		corRoad: 0,
		objectiveRoad:"Road 2",
		x: "0",
		y: "0",
		paths: function(legendIndex)
		{
		 return `<path onclick="nextTask($(this))" pathID="0" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][1].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 1317.8202,919.87232 -36.0224,-2.00124 -57.0355,-13.0081 -56.0349,-13.00809 -57.0355,-3.00187 -39.0243,6.00373 -56.0348,16.00997 -52.03242,6.00374 -56.03487,-4.00249 -50.03114,-5.00312 -50.03114,6.00374 -101.06291,30.01868 -103.06415,51.03177"/>
		 <path onclick="nextTask($(this))" pathID="0" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][1].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 1317.8202,919.87232 -36.0224,-2.00124 -57.0355,-13.0081 -56.0349,-13.00809 -57.0355,-3.00187 -39.0243,6.00373 -56.0348,16.00997 -52.03242,6.00374 -56.03487,-4.00249 -50.03114,-5.00312 -50.03114,6.00374 -101.06291,30.01868 -103.06415,51.03177"/>
 		 <path onclick="nextTask($(this))" pathID="1" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][4].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 796.49575,53.332976 -27.01681,31.019307 -34.02118,37.023047 -43.02678,42.02615 -12.00747,9.00561 -20.01246,2.00125 -23.01432,-3.00187 -17.01059,-3.00187 -18.01121,0 -43.02678,2.00124 -66.04111,7.00436 -60.03737,8.00499 -48.02989,13.00809 -63.03924,35.0218 -37.02304,27.01682 -24.01495,29.01806"/>
 		 <path onclick="nextTask($(this))" pathID="1" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][4].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 796.49575,53.332976 -27.01681,31.019307 -34.02118,37.023047 -43.02678,42.02615 -12.00747,9.00561 -20.01246,2.00125 -23.01432,-3.00187 -17.01059,-3.00187 -18.01121,0 -43.02678,2.00124 -66.04111,7.00436 -60.03737,8.00499 -48.02989,13.00809 -63.03924,35.0218 -37.02304,27.01682 -24.01495,29.01806"/>
 		 <path onclick="nextTask($(this))" pathID="2" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][2].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 541.98113,492.45283 24.05661,-36.79245 25.47169,-38.20755 19.81133,-16.98113 33.96226,-21.22642 28.30189,-12.73585 22.64151,-5.66037 55.18868,4.24528 33.96226,1.41509 32.54717,1.4151 45.28302,11.32075 52.35849,11.32076 227.83016,55.18868 16.9812,1.41509 22.6415,0"/>
 		 <path onclick="nextTask($(this))" pathID="2" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][2].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 541.98113,492.45283 24.05661,-36.79245 25.47169,-38.20755 19.81133,-16.98113 33.96226,-21.22642 28.30189,-12.73585 22.64151,-5.66037 55.18868,4.24528 33.96226,1.41509 32.54717,1.4151 45.28302,11.32075 52.35849,11.32076 227.83016,55.18868 16.9812,1.41509 22.6415,0"/>`;
		}
	},
	{
		// 3
		img: "img/14/3.png",
		corRoad: 0,
		objectiveRoad:"Road 6",
		x: "0",
		y: "0",
		paths: function(legendIndex)
		{
			return `<path onclick="nextTask($(this))" pathID="0" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][5].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 814.50696,950.89163 106.06602,-47.02927 121.07532,-64.03986 94.0586,-72.04484 83.0517,-67.04173 54.0336,-41.02553 45.028,-27.01682 24.015,-13.0081"/>
			<path onclick="nextTask($(this))" pathID="0" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][5].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 814.50696,950.89163 106.06602,-47.02927 121.07532,-64.03986 94.0586,-72.04484 83.0517,-67.04173 54.0336,-41.02553 45.028,-27.01682 24.015,-13.0081"/>
 		 <path onclick="nextTask($(this))" pathID="1" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][6].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 584.36372,636.69607 81.05045,110.06851 35.02179,-39.02429 14.00872,-14.00872 11.00685,-21.01308 26.0162,-121.07536 0,-5.00311 -2.00125,-5.00312 -2.00124,-3.00186 7.00435,-6.00374 4.0025,-12.00747 4.00249,-26.0162 16.00996,-43.02678 0,-25.01557 8.00498,-28.01744 13.0081,-32.01993 11.00685,-19.01183 20.01246,-15.00934"/>
 		 <path onclick="nextTask($(this))" pathID="1" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][6].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 584.36372,636.69607 81.05045,110.06851 35.02179,-39.02429 14.00872,-14.00872 11.00685,-21.01308 26.0162,-121.07536 0,-5.00311 -2.00125,-5.00312 -2.00124,-3.00186 7.00435,-6.00374 4.0025,-12.00747 4.00249,-26.0162 16.00996,-43.02678 0,-25.01557 8.00498,-28.01744 13.0081,-32.01993 11.00685,-19.01183 20.01246,-15.00934"/>
 		 <path onclick="nextTask($(this))" pathID="2" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][1].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 1422.8774,300.70755 -6.368,0 -5.6603,0 -16.2736,42.45283 -62.9717,-26.8868 -24.0566,-9.19811 -28.3019,-5.66038 -13.4434,-3.53773 -99.7642,14.85849 -11.3207,4.95283 -6.3679,-2.12264 0.7075,-12.02831 -4.9528,-19.81132 -0.7076,-24.0566 -2.8302,-9.90566 -2.8301,-4.24528 33.9622,-12.73585 22.6415,-10.61321 26.1793,-12.73585 21.9339,-10.61321 13.4434,-4.95283 14.8585,30.42453 38.2076,7.07547 0,0"/>
 		 <path onclick="nextTask($(this))" pathID="2" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][1].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 1422.8774,300.70755 -6.368,0 -5.6603,0 -16.2736,42.45283 -62.9717,-26.8868 -24.0566,-9.19811 -28.3019,-5.66038 -13.4434,-3.53773 -99.7642,14.85849 -11.3207,4.95283 -6.3679,-2.12264 0.7075,-12.02831 -4.9528,-19.81132 -0.7076,-24.0566 -2.8302,-9.90566 -2.8301,-4.24528 33.9622,-12.73585 22.6415,-10.61321 26.1793,-12.73585 21.9339,-10.61321 13.4434,-4.95283 14.8585,30.42453 38.2076,7.07547 0,0"/>`;
		}
	},
	
	{
		// 4
		img: "img/14/4.png",
		corRoad: 0,
		objectiveRoad:"Road 1",
		x: "0",
		y: "0",
		paths: function(legendIndex)
		{
			return `<path onclick="nextTask($(this))" pathID="0" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][0].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 192.11958,56.334844 26.01619,12.007474 39.02429,0 23.01433,-4.002491 28.01743,5.003114 34.02118,10.006228 32.01993,-2.001246 69.04297,9.005606 38.02367,6.003736 33.02055,21.013075 73.04547,56.03488 14.00872,10.00623 6.00373,-10.00623 11.00685,-6.00374 25.01557,1.00063 0,-20.01246 4.00249,-24.01495 -1.00062,-15.00934 -2.00124,-6.003735 19.01183,-5.003114"/>
			<path onclick="nextTask($(this))" pathID="0" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][0].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 192.11958,56.334844 26.01619,12.007474 39.02429,0 23.01433,-4.002491 28.01743,5.003114 34.02118,10.006228 32.01993,-2.001246 69.04297,9.005606 38.02367,6.003736 33.02055,21.013075 73.04547,56.03488 14.00872,10.00623 6.00373,-10.00623 11.00685,-6.00374 25.01557,1.00063 0,-20.01246 4.00249,-24.01495 -1.00062,-15.00934 -2.00124,-6.003735 19.01183,-5.003114"/>
 		 <path onclick="nextTask($(this))" pathID="1" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][7].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 123.07661,205.42764 70.04359,12.00748 56.03488,28.01743 74.04609,47.02928 119.07411,75.04671 113.07038,72.04484 81.05044,41.02553 79.04921,17.01059 88.0548,7.00436"/>
 		 <path onclick="nextTask($(this))" pathID="1" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][7].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 123.07661,205.42764 70.04359,12.00748 56.03488,28.01743 74.04609,47.02928 119.07411,75.04671 113.07038,72.04484 81.05044,41.02553 79.04921,17.01059 88.0548,7.00436"/>
 		 <path onclick="nextTask($(this))" pathID="2" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][5].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 874.54433,709.74154 -15.00934,8.00498 -52.03239,-10.00623 -42.02615,-7.00436 -36.02243,-7.00436 -27.01681,-8.00498 -33.02055,-14.00872 -31.01931,-13.0081 -12.00747,-1.00062 -17.01059,0 -26.01619,5.00311 -30.01869,9.00561 -44.0274,-3.00187 -35.0218,-6.00374 -35.0218,-3.00186 -31.01931,-2.00125 -21.01307,-4.00249 -16.00997,-7.00436 -10.00623,-15.00934 -39.02429,-66.04111 -17.01058,-20.01245 -34.02118,-13.0081 -36.02242,-11.00685 -16.00997,-4.00249 -30.01868,4.00249 -37.02304,4.00249 -40.02492,-5.00311 -26.016189,-11.00686 -32.01993,-4.00249 -12.007473,-4.00249"/>
 		 <path onclick="nextTask($(this))" pathID="2" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][5].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 874.54433,709.74154 -15.00934,8.00498 -52.03239,-10.00623 -42.02615,-7.00436 -36.02243,-7.00436 -27.01681,-8.00498 -33.02055,-14.00872 -31.01931,-13.0081 -12.00747,-1.00062 -17.01059,0 -26.01619,5.00311 -30.01869,9.00561 -44.0274,-3.00187 -35.0218,-6.00374 -35.0218,-3.00186 -31.01931,-2.00125 -21.01307,-4.00249 -16.00997,-7.00436 -10.00623,-15.00934 -39.02429,-66.04111 -17.01058,-20.01245 -34.02118,-13.0081 -36.02242,-11.00685 -16.00997,-4.00249 -30.01868,4.00249 -37.02304,4.00249 -40.02492,-5.00311 -26.016189,-11.00686 -32.01993,-4.00249 -12.007473,-4.00249"/>`;
		}
	},
	{
		// 5
		img: "img/14/5.png",
		corRoad: 1,
		objectiveRoad:"Road 2",
		x: "0",
		y: "0",
		paths: function(legendIndex)
		{
			return `<path onclick="nextTask($(this))" pathID="0" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][3].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 791.74528,536.32075 29.71699,99.05661 19.10377,63.67924 21.93396,49.52831 19.10377,19.81132 19.10378,9.90566 31.83962,5.66037 38.20755,4.95283 29.00943,13.4434 14.85845,8.49057 21.2265,26.17924 31.132,31.83962 26.1793,28.30189 19.8113,24.0566 4.2453,14.8585"/>
			<path onclick="nextTask($(this))" pathID="0" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][3].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 791.74528,536.32075 29.71699,99.05661 19.10377,63.67924 21.93396,49.52831 19.10377,19.81132 19.10378,9.90566 31.83962,5.66037 38.20755,4.95283 29.00943,13.4434 14.85845,8.49057 21.2265,26.17924 31.132,31.83962 26.1793,28.30189 19.8113,24.0566 4.2453,14.8585"/>
 		 <path onclick="nextTask($(this))" pathID="1" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][1].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 1172.0519,358.72641 -14.5047,3.18397 -53.0661,-11.32076 -34.316,7.07547 -3.8915,-8.49056 -7.4293,-12.38208 -2.4764,-4.24528 -6.7217,-2.83019 -4.599,-7.78302 -12.7359,-26.53302 -44.22168,8.49057 6.36793,-10.96698 2.47641,-13.4434 1.76887,-21.93396 1.76887,-12.38208 1.4151,-4.95283 5.6604,-9.55188 9.1981,-12.73585 7.783,-5.66038 14.1509,-1.06132 14.5048,-12.73585 9.1981,-12.73585 -0.3538,-6.36792 -7.0755,-14.50472 6.0142,-22.28774 5.6604,-15.21226 0,-10.25944 8.8443,-1.41509 6.3679,-4.24528 6.3679,-4.24529 6.7217,0 5.3067,-7.783015 0,0 0,-0.353773"/>
 		 <path onclick="nextTask($(this))" pathID="1" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][1].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 1172.0519,358.72641 -14.5047,3.18397 -53.0661,-11.32076 -34.316,7.07547 -3.8915,-8.49056 -7.4293,-12.38208 -2.4764,-4.24528 -6.7217,-2.83019 -4.599,-7.78302 -12.7359,-26.53302 -44.22168,8.49057 6.36793,-10.96698 2.47641,-13.4434 1.76887,-21.93396 1.76887,-12.38208 1.4151,-4.95283 5.6604,-9.55188 9.1981,-12.73585 7.783,-5.66038 14.1509,-1.06132 14.5048,-12.73585 9.1981,-12.73585 -0.3538,-6.36792 -7.0755,-14.50472 6.0142,-22.28774 5.6604,-15.21226 0,-10.25944 8.8443,-1.41509 6.3679,-4.24528 6.3679,-4.24529 6.7217,0 5.3067,-7.783015 0,0 0,-0.353773"/>
 		 <path onclick="nextTask($(this))" pathID="2" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][4].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 564.35126,314.49553 -4.00249,-8.00498 -0.50031,-7.50468 0.50031,-5.50342 -28.01744,-31.01931 -14.50903,-17.01059 -12.50778,-16.51027 -7.50467,-13.50841 -8.5053,-6.50405 -12.00747,-3.00187 -20.01246,2.00125 -16.00996,-1.50094 -97.06041,-25.51588 -12.00748,-7.50467 -8.50529,-11.50716 -7.00436,-9.50592 -4.5028,-6.00373 -7.50468,-4.50281 -38.02366,-11.50716 -22.51402,-9.50592 -10.50653,-3.50218 -4.50281,-0.50031 -18.51152,8.5053 -13.0081,8.50529 -17.01058,8.5053 -19.51215,8.00498 0,0.50031"/>
 		 <path onclick="nextTask($(this))" pathID="2" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][4].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 564.35126,314.49553 -4.00249,-8.00498 -0.50031,-7.50468 0.50031,-5.50342 -28.01744,-31.01931 -14.50903,-17.01059 -12.50778,-16.51027 -7.50467,-13.50841 -8.5053,-6.50405 -12.00747,-3.00187 -20.01246,2.00125 -16.00996,-1.50094 -97.06041,-25.51588 -12.00748,-7.50467 -8.50529,-11.50716 -7.00436,-9.50592 -4.5028,-6.00373 -7.50468,-4.50281 -38.02366,-11.50716 -22.51402,-9.50592 -10.50653,-3.50218 -4.50281,-0.50031 -18.51152,8.5053 -13.0081,8.50529 -17.01058,8.5053 -19.51215,8.00498 0,0.50031"/>`;
		}
	},
	{
		// 6
		img: "img/15/0.png",
		corRoad: 1,
		objectiveRoad:"Road 4",
		x: "0",
		y: "0",
		paths: function(legendIndex)
		{
			return `<path onclick="nextTask($(this))" pathID="0" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][6].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 1186.7386,794.79447 -27.0168,40.02492 -16.0099,15.00934 -24.015,26.01619 -30.0187,35.0218 -6.0037,1.00062 -5.0031,4.00249 -3.0019,6.00374 -31.0193,9.00561 -101.06289,17.01058 -15.00935,2.00125 -48.02989,-16.00997 -33.02055,-21.01307 -8.00499,-10.00623 1.00063,-6.00374 -2.00125,-5.00311 21.01308,-12.00748 24.01495,-25.01557 9.0056,-15.00934 3.00187,-17.01059 -4.00249,-22.0137 -6.00374,-32.01993 0,-25.01557 6.00374,-17.01059 12.00747,-15.00934 19.01184,-20.01245 18.01121,-16.00997 7.00436,-6.00374 12.00747,-5.00311"/>
			<path onclick="nextTask($(this))" pathID="0" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][6].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 1186.7386,794.79447 -27.0168,40.02492 -16.0099,15.00934 -24.015,26.01619 -30.0187,35.0218 -6.0037,1.00062 -5.0031,4.00249 -3.0019,6.00374 -31.0193,9.00561 -101.06289,17.01058 -15.00935,2.00125 -48.02989,-16.00997 -33.02055,-21.01307 -8.00499,-10.00623 1.00063,-6.00374 -2.00125,-5.00311 21.01308,-12.00748 24.01495,-25.01557 9.0056,-15.00934 3.00187,-17.01059 -4.00249,-22.0137 -6.00374,-32.01993 0,-25.01557 6.00374,-17.01059 12.00747,-15.00934 19.01184,-20.01245 18.01121,-16.00997 7.00436,-6.00374 12.00747,-5.00311"/>
 		 <path onclick="nextTask($(this))" pathID="1" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][3].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 324.20179,617.68424 34.02117,51.03176 40.02492,22.0137 29.01806,-31.0193 9.0056,-15.00935 16.00997,-9.0056 22.0137,-8.00498 6.00374,-2.00125 -4.00249,-12.00747 -12.00748,-20.01246 -10.00623,-25.01557 -2.00124,-41.02553 6.00373,-17.01059 10.00623,-13.0081 23.01433,-10.00623 17.01058,-11.00685 9.00561,-9.0056 11.00685,-3.00187 1.00062,-5.00312 12.00748,-20.01245 15.00934,-20.01246 20.01246,7.00436 32.01993,-12.00747 15.00934,-15.00934 4.00249,-5.00312 9.0056,-3.00187 9.00561,-16.00996 1.00062,-9.00561 -4.00249,-11.00685 6.00374,-11.00685 0,0"/>
 		 <path onclick="nextTask($(this))" pathID="1" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][3].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 324.20179,617.68424 34.02117,51.03176 40.02492,22.0137 29.01806,-31.0193 9.0056,-15.00935 16.00997,-9.0056 22.0137,-8.00498 6.00374,-2.00125 -4.00249,-12.00747 -12.00748,-20.01246 -10.00623,-25.01557 -2.00124,-41.02553 6.00373,-17.01059 10.00623,-13.0081 23.01433,-10.00623 17.01058,-11.00685 9.00561,-9.0056 11.00685,-3.00187 1.00062,-5.00312 12.00748,-20.01245 15.00934,-20.01246 20.01246,7.00436 32.01993,-12.00747 15.00934,-15.00934 4.00249,-5.00312 9.0056,-3.00187 9.00561,-16.00996 1.00062,-9.00561 -4.00249,-11.00685 6.00374,-11.00685 0,0"/>
 		 <path onclick="nextTask($(this))" pathID="2" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][0].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 1095.682,434.57026 18.0112,-70.04359 43.0268,34.02117 10.0062,-14.00872 21.0131,-29.01806 75.0467,-148.09217 -48.0299,-44.02741 -57.0355,-39.02428 -61.038,117.07286 -16.01,-3.00187 -4.0025,13.0081 -6.0037,10.00623 -6.0037,10.00623 -1.0007,9.0056 -108.06722,-15.00934 -29.01806,88.05481"/>
 		 <path onclick="nextTask($(this))" pathID="2" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][0].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 1095.682,434.57026 18.0112,-70.04359 43.0268,34.02117 10.0062,-14.00872 21.0131,-29.01806 75.0467,-148.09217 -48.0299,-44.02741 -57.0355,-39.02428 -61.038,117.07286 -16.01,-3.00187 -4.0025,13.0081 -6.0037,10.00623 -6.0037,10.00623 -1.0007,9.0056 -108.06722,-15.00934 -29.01806,88.05481"/>`;	
		}
	},
	{
		// 7
		img: "img/15/1.png",
		corRoad: 0,
		objectiveRoad:"Road 1",
		x: "0",
		y: "0",
		paths: function(legendIndex)
		{
			return `<path onclick="nextTask($(this))" pathID="0" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][0].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 1210.7536,923.87482 -21.0131,-27.01682 -18.0112,-12.00747 -8.005,-1.00063 -18.0112,0 -22.0137,9.00561 -15.0093,11.00685 -14.0088,18.01121 -24.0149,32.01993 -24.0149,30.01868 -21.0131,17.01062 -39.02431,6.0037 -50.03114,-2.0012 -40.02492,-1.0007 -51.03176,-3.0018 -61.03799,-1.00065 -33.02055,0 -38.02367,4.00245 -23.01432,10.0063 -20.01246,15.0093 -21.01308,23.0143 -26.01619,20.0125 -24.01495,15.0093"/>
			<path onclick="nextTask($(this))" pathID="0" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][0].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 1210.7536,923.87482 -21.0131,-27.01682 -18.0112,-12.00747 -8.005,-1.00063 -18.0112,0 -22.0137,9.00561 -15.0093,11.00685 -14.0088,18.01121 -24.0149,32.01993 -24.0149,30.01868 -21.0131,17.01062 -39.02431,6.0037 -50.03114,-2.0012 -40.02492,-1.0007 -51.03176,-3.0018 -61.03799,-1.00065 -33.02055,0 -38.02367,4.00245 -23.01432,10.0063 -20.01246,15.0093 -21.01308,23.0143 -26.01619,20.0125 -24.01495,15.0093"/>
 		 <path onclick="nextTask($(this))" pathID="1" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][1].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 874.54433,469.59206 -12.00747,17.01059 -8.00498,12.00747 0,13.0081 8.00498,15.00934 12.00747,12.00748 17.01059,8.00498 18.01121,9.0056 7.00436,18.01121 -4.00249,13.0081 -14.00872,28.01744 -18.01121,18.01121 -18.01121,15.00934 -19.01184,9.00561 -76.04733,1.00062 -8.00498,-152.09467 3.00187,-35.02179 12.00747,-32.01993 17.01059,-24.01495 19.01183,-16.00997 27.01682,-13.00809 36.02242,-11.00685 125.07785,-40.02492" />
 		 <path onclick="nextTask($(this))" pathID="1" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][1].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 874.54433,469.59206 -12.00747,17.01059 -8.00498,12.00747 0,13.0081 8.00498,15.00934 12.00747,12.00748 17.01059,8.00498 18.01121,9.0056 7.00436,18.01121 -4.00249,13.0081 -14.00872,28.01744 -18.01121,18.01121 -18.01121,15.00934 -19.01184,9.00561 -76.04733,1.00062 -8.00498,-152.09467 3.00187,-35.02179 12.00747,-32.01993 17.01059,-24.01495 19.01183,-16.00997 27.01682,-13.00809 36.02242,-11.00685 125.07785,-40.02492" />
 		 <path onclick="nextTask($(this))" pathID="2" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][2].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 1184.434,639.62264 66.5094,28.30189 59.434,1.41509 50.9434,-5.66038 18.3962,-9.90566 22.6415,-18.39622 12.7358,-28.30189 11.3208,-24.0566 -60.8491,-29.71698 -19.8113,-12.73585 L 1335.8491,525 1334.434,509.43396 1350,427.35849 l 1.4151,-31.13208 -2.8302,-59.43396 L 1328.7736,300"/>
 		 <path onclick="nextTask($(this))" pathID="2" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][2].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 1184.434,639.62264 66.5094,28.30189 59.434,1.41509 50.9434,-5.66038 18.3962,-9.90566 22.6415,-18.39622 12.7358,-28.30189 11.3208,-24.0566 -60.8491,-29.71698 -19.8113,-12.73585 L 1335.8491,525 1334.434,509.43396 1350,427.35849 l 1.4151,-31.13208 -2.8302,-59.43396 L 1328.7736,300"/>`;
		}
	},
	{
		// 8
		img: "img/15/2.png",
		corRoad: 2,
		objectiveRoad:"Road 8",
		x: "0",
		y: "0",
		paths: function(legendIndex)
		{
			return `<path onclick="nextTask($(this))" pathID="0" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][5].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="M 876.03591,1037.0247 890.90498,832.575 877.275,802.83686 l -30.97722,-17.34724 -45.8463,-22.3036 -52.04173,-7.43454 -8.67363,0 -55.759,2.47818 -33.4554,4.95636 -16.10816,4.95635 -45.84629,28.49905 -30.97723,23.54269 -16.10815,-12.39089 -8.67363,-3.71727 -2.47817,-21.06451 0,0"/>
			<path onclick="nextTask($(this))" pathID="0" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][5].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="M 876.03591,1037.0247 890.90498,832.575 877.275,802.83686 l -30.97722,-17.34724 -45.8463,-22.3036 -52.04173,-7.43454 -8.67363,0 -55.759,2.47818 -33.4554,4.95636 -16.10816,4.95635 -45.84629,28.49905 -30.97723,23.54269 -16.10815,-12.39089 -8.67363,-3.71727 -2.47817,-21.06451 0,0"/>
 		 <path onclick="nextTask($(this))" pathID="1" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][6].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 1135.0055,791.68506 0,-11.1518 -35.9336,-125.14799 -11.1518,-40.88993 -2.4781,-23.54269 1.239,-50.80265 -13.6299,-24.78178 1.2391,-4.95636 2.4781,-49.56356 8.6737,-13.62998 16.1081,-9.91271 -12.3909,-14.86907 -35.9336,-4.95635 -4.9563,-3.71727 3.7172,1.23909" />
 		 <path onclick="nextTask($(this))" pathID="1" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][6].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 1135.0055,791.68506 0,-11.1518 -35.9336,-125.14799 -11.1518,-40.88993 -2.4781,-23.54269 1.239,-50.80265 -13.6299,-24.78178 1.2391,-4.95636 2.4781,-49.56356 8.6737,-13.62998 16.1081,-9.91271 -12.3909,-14.86907 -35.9336,-4.95635 -4.9563,-3.71727 3.7172,1.23909" />
 		 <path onclick="nextTask($(this))" pathID="2" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][7].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 758.32246,318.35307 -6.19545,18.58633 -23.54269,18.58634 -26.02086,12.39089 -37.17267,11.1518 -19.82543,8.67362 -3.71727,4.95636 0,6.19544 6.19545,12.39089 -40.88994,13.62998 -34.69449,13.62998 -79.30169,59.47627 -61.95445,53.28083 -7.43454,-1.23909 -16.10815,1.23909 -26.02087,6.19544 -17.34725,1.23909 -3.71727,-2.47817 -1.23908,-9.91272 4.95635,-16.10815 11.1518,1.23909 17.34725,0 21.06451,-4.95636"/>
 		 <path onclick="nextTask($(this))" pathID="2" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][7].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 758.32246,318.35307 -6.19545,18.58633 -23.54269,18.58634 -26.02086,12.39089 -37.17267,11.1518 -19.82543,8.67362 -3.71727,4.95636 0,6.19544 6.19545,12.39089 -40.88994,13.62998 -34.69449,13.62998 -79.30169,59.47627 -61.95445,53.28083 -7.43454,-1.23909 -16.10815,1.23909 -26.02087,6.19544 -17.34725,1.23909 -3.71727,-2.47817 -1.23908,-9.91272 4.95635,-16.10815 11.1518,1.23909 17.34725,0 21.06451,-4.95636"/>`;	
		}
	},
	{
		// 9
		img: "img/15/3.png",
		corRoad: 2,
		objectiveRoad:"Road 8",
		x: "0",
		y: "0",
		paths: function(legendIndex)
		{
			return `<path onclick="nextTask($(this))" pathID="0" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][3].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 1112.2642,721.69811 -25.4717,4.24529 -11.3208,-4.24529 -65.0943,-39.62264 -24.05664,-25.4717 -49.52831,-69.33962 -70.75471,-42.45283 -127.35849,-63.67925 38.20754,-24.0566 56.60378,-14.15094 22.64151,-1.4151"/>
			<path onclick="nextTask($(this))" pathID="0" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][3].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 1112.2642,721.69811 -25.4717,4.24529 -11.3208,-4.24529 -65.0943,-39.62264 -24.05664,-25.4717 -49.52831,-69.33962 -70.75471,-42.45283 -127.35849,-63.67925 38.20754,-24.0566 56.60378,-14.15094 22.64151,-1.4151"/>
 		 <path onclick="nextTask($(this))" pathID="1" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][4].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 1030.1887,1030.1887 -24.0566,-49.52832 -11.32078,-7.07547 -35.37736,-9.90566 -15.56603,-11.32076 -16.98114,-15.56604 -14.15094,-18.39622 -15.56604,-24.05661 -9.90566,-9.90566 -11.32075,-4.24528 -16.98113,-1.4151 -41.03774,0 -9.90566,26.8868 1.41509,21.22641 -4.24528,8.49057 -8.49057,5.66038 -9.90566,-4.24529 -5.66037,-4.24528 -7.07548,-24.0566 -15.56603,-19.81132 -5.66038,-15.56604 5.66038,-21.22642 -19.81132,0 -21.22642,-29.71698 0,0"/>
 		 <path onclick="nextTask($(this))" pathID="1" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][4].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 1030.1887,1030.1887 -24.0566,-49.52832 -11.32078,-7.07547 -35.37736,-9.90566 -15.56603,-11.32076 -16.98114,-15.56604 -14.15094,-18.39622 -15.56604,-24.05661 -9.90566,-9.90566 -11.32075,-4.24528 -16.98113,-1.4151 -41.03774,0 -9.90566,26.8868 1.41509,21.22641 -4.24528,8.49057 -8.49057,5.66038 -9.90566,-4.24529 -5.66037,-4.24528 -7.07548,-24.0566 -15.56603,-19.81132 -5.66038,-15.56604 5.66038,-21.22642 -19.81132,0 -21.22642,-29.71698 0,0"/>
 		 <path onclick="nextTask($(this))" pathID="2" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][7].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 1359.9057,544.81132 -93.3963,-60.84906 -32.5471,-16.98113 -39.6227,-12.73585 8.4906,-49.5283 -1.4151,-32.54717 -11.3208,-41.03774 -11.3207,-21.22641 -12.7359,-38.20755 -43.8679,21.22642 -5.6604,-22.64151 0,-9.90566 11.3208,-24.05661 36.7924,-59.43396 0,0"/>
 		 <path onclick="nextTask($(this))" pathID="2" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][7].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 1359.9057,544.81132 -93.3963,-60.84906 -32.5471,-16.98113 -39.6227,-12.73585 8.4906,-49.5283 -1.4151,-32.54717 -11.3208,-41.03774 -11.3207,-21.22641 -12.7359,-38.20755 -43.8679,21.22642 -5.6604,-22.64151 0,-9.90566 11.3208,-24.05661 36.7924,-59.43396 0,0"/>`;	
		}
	},
	{
		// 10
		img: "img/15/4.png",
		corRoad: 1,
		objectiveRoad:"Road 3",
		x: "0",
		y: "0",
		paths: function(legendIndex)
		{
			return `<path onclick="nextTask($(this))" pathID="0" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][6].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 526.3276,625.68922 -11.00686,-41.02553 18.01122,-8.00499 14.00871,-15.00934 7.00436,-18.01121 1.00063,-47.02927 4.00249,-18.01121 7.00436,-16.00997 12.00747,-15.00934 9.00561,-15.00934 1.00062,-7.00436 27.01682,14.00872 18.01121,15.00934 31.0193,11.00685 42.02616,19.01183 -9.0056,37.02305 -11.00686,24.01495 2.00125,11.00685 19.01183,45.02802 10.00623,1.00063"/>
			<path onclick="nextTask($(this))" pathID="0" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][6].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 526.3276,625.68922 -11.00686,-41.02553 18.01122,-8.00499 14.00871,-15.00934 7.00436,-18.01121 1.00063,-47.02927 4.00249,-18.01121 7.00436,-16.00997 12.00747,-15.00934 9.00561,-15.00934 1.00062,-7.00436 27.01682,14.00872 18.01121,15.00934 31.0193,11.00685 42.02616,19.01183 -9.0056,37.02305 -11.00686,24.01495 2.00125,11.00685 19.01183,45.02802 10.00623,1.00063"/>
 		 <path onclick="nextTask($(this))" pathID="1" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][2].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 784.66981,1045.7547 14.85849,-7.0755 1.4151,-4.9528 4.24528,0 0,-16.2736 -23.34906,-84.19808 0.70755,-43.16038 12.73585,-2.83019 12.73585,-7.78302 6.36792,-5.66038 19.10378,-33.25471 19.10377,-29.00944 6.36793,-7.78302 6.36792,-3.53773 12.73585,2.83019 14.85849,-0.70755 14.15094,-8.49057 14.15095,-7.78301 11.32075,0 14.15094,5.66037 9.19812,2.83019 14.85849,0 4.95283,4.95283 4.95283,-2.12264 2.12264,-3.53774 33.25468,-10.6132 11.3208,-4.24529 7.0755,-9.19811 9.9056,0"/>
 		 <path onclick="nextTask($(this))" pathID="1" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][2].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 784.66981,1045.7547 14.85849,-7.0755 1.4151,-4.9528 4.24528,0 0,-16.2736 -23.34906,-84.19808 0.70755,-43.16038 12.73585,-2.83019 12.73585,-7.78302 6.36792,-5.66038 19.10378,-33.25471 19.10377,-29.00944 6.36793,-7.78302 6.36792,-3.53773 12.73585,2.83019 14.85849,-0.70755 14.15094,-8.49057 14.15095,-7.78301 11.32075,0 14.15094,5.66037 9.19812,2.83019 14.85849,0 4.95283,4.95283 4.95283,-2.12264 2.12264,-3.53774 33.25468,-10.6132 11.3208,-4.24529 7.0755,-9.19811 9.9056,0"/>
 		 <path onclick="nextTask($(this))" pathID="2" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][1].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 1001.6234,503.61324 -9.00558,-46.02865 -9.0056,-39.02429 -4.00249,-36.02242 21.01307,-1.00062 12.0075,-5.00312 17.0106,-2.00124 1.0006,-49.03052 -5.0031,-14.00872 -1.0006,-8.00498 0,-9.00561 4.0024,-7.00436 8.005,-5.00311 8.005,-1.00062 4.0025,-45.02803 4.0025,-30.01868 10.0062,-24.01495 7.0044,-17.01059 3.0018,-11.00685 20.0125,2.00124 19.0118,4.0025 1.0007,27.01681"/>
 		 <path onclick="nextTask($(this))" pathID="2" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][1].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 1001.6234,503.61324 -9.00558,-46.02865 -9.0056,-39.02429 -4.00249,-36.02242 21.01307,-1.00062 12.0075,-5.00312 17.0106,-2.00124 1.0006,-49.03052 -5.0031,-14.00872 -1.0006,-8.00498 0,-9.00561 4.0024,-7.00436 8.005,-5.00311 8.005,-1.00062 4.0025,-45.02803 4.0025,-30.01868 10.0062,-24.01495 7.0044,-17.01059 3.0018,-11.00685 20.0125,2.00124 19.0118,4.0025 1.0007,27.01681"/>`;	
		}		
	},
	{
		// 11
		img: "img/15/5.png",
		corRoad: 1,
		objectiveRoad:"Road 4",
		x: "0",
		y: "0",
		paths: function(legendIndex)
		{
			return `<path onclick="nextTask($(this))" pathID="0" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][0].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 811.50509,457.58459 2.00125,-71.04422 6.00374,-21.01308 4.00249,-74.04609 75.04671,-8.00498 0,-9.0056 32.01993,-3.00187 25.01557,7.00436 18.01121,15.00934 16.00996,32.01993 25.01555,53.03301 32.02,49.03051 20.0124,15.00935 22.0137,27.01681 36.0224,63.03924 22.0137,-14.00872 3.0019,-11.00685 -25.0156,-37.02304"/>
			<path onclick="nextTask($(this))" pathID="0" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][0].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 811.50509,457.58459 2.00125,-71.04422 6.00374,-21.01308 4.00249,-74.04609 75.04671,-8.00498 0,-9.0056 32.01993,-3.00187 25.01557,7.00436 18.01121,15.00934 16.00996,32.01993 25.01555,53.03301 32.02,49.03051 20.0124,15.00935 22.0137,27.01681 36.0224,63.03924 22.0137,-14.00872 3.0019,-11.00685 -25.0156,-37.02304"/>
 	 	 <path onclick="nextTask($(this))" pathID="1" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][3].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 1003.6247,895.85738 -4.00252,-54.03363 0,-23.01433 50.03112,-35.0218 -6.0037,-48.02989 -1.0006,-24.01495 12.0074,-2.00124 35.0218,16.00996 41.0256,19.01183 42.0261,9.00561 45.0281,-4.00249 88.0548,-36.02242 106.066,-48.0299 -16.01,-34.02117" />
 	 	 <path onclick="nextTask($(this))" pathID="1" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][3].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 1003.6247,895.85738 -4.00252,-54.03363 0,-23.01433 50.03112,-35.0218 -6.0037,-48.02989 -1.0006,-24.01495 12.0074,-2.00124 35.0218,16.00996 41.0256,19.01183 42.0261,9.00561 45.0281,-4.00249 88.0548,-36.02242 106.066,-48.0299 -16.01,-34.02117" />
 		 <path onclick="nextTask($(this))" pathID="2" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][7].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 794.49451,628.69109 -49.03052,4.00249 2.00125,49.03052 -24.01495,10.00623 -34.02118,7.00436 -26.01619,0 -12.00747,0 -4.0025,-4.0025 -4.00249,0 -3.00186,2.00125 -21.01308,0 -1.00063,32.01993 9.00561,105.06539 4.00249,45.02803 8.00498,24.01495 11.00685,30.01868 15.00935,34.02118 7.00435,39.0243 -19.01183,0 -10.00623,-7.00437 -5.00311,-9.00561 -7.00436,-31.01931"/>	
 		 <path onclick="nextTask($(this))" pathID="2" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][7].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 794.49451,628.69109 -49.03052,4.00249 2.00125,49.03052 -24.01495,10.00623 -34.02118,7.00436 -26.01619,0 -12.00747,0 -4.0025,-4.0025 -4.00249,0 -3.00186,2.00125 -21.01308,0 -1.00063,32.01993 9.00561,105.06539 4.00249,45.02803 8.00498,24.01495 11.00685,30.01868 15.00935,34.02118 7.00435,39.0243 -19.01183,0 -10.00623,-7.00437 -5.00311,-9.00561 -7.00436,-31.01931"/>`;	
		}
	},
	//Daltonized colors:
	// for colors: use: legend[dalt][n].color
	{
		// 0
		img: "img/14/0.png",
		corRoad: 1,
		objectiveRoad:"Road 4",
		x: "0",
		y: "0",
		
		paths : function(legendIndex)
		{
			return	`<path onclick="nextTask($(this))" pathID="0" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][2].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 807.8271,815.71262 29.78972,-30.66589 21.90421,-28.91355 21.02804,-34.17056 23.65654,-28.91355 28.91355,-28.91356 35.9229,-24.53271 42.93224,-24.53271 49.9416,-18.39953 63.0841,-14.89486 78.8551,-16.6472 73.5982,-17.52336 45.5607,-8.76168 30.6659,-2.62851 0,0"/> 
			<path onclick="nextTask($(this))" pathID="0" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][2].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 807.8271,815.71262 29.78972,-30.66589 21.90421,-28.91355 21.02804,-34.17056 23.65654,-28.91355 28.91355,-28.91356 35.9229,-24.53271 42.93224,-24.53271 49.9416,-18.39953 63.0841,-14.89486 78.8551,-16.6472 73.5982,-17.52336 45.5607,-8.76168 30.6659,-2.62851 0,0"/> 
			<path onclick="nextTask($(this))" pathID="1" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][3].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 746.55111,556.8777 -19.20587,0.61954 -12.39089,0.61955 -8.05408,-3.71727 -14.24953,-9.29317 -22.3036,-14.86906 -19.82542,-12.39089 -21.68406,-9.91272 -22.92315,-9.29316 -25.40132,-9.29317 -18.58633,-8.05408 -16.10816,-4.95635 -9.91271,-3.71727 -10.53226,2.47818 -13.01043,3.71726 -15.48862,4.33681 -9.29316,0.61955 -6.81499,-1.85863 -6.19545,-3.71727 -8.67362,-14.86907 -8.67362,-17.34724 -10.53226,-22.30361 -5.5759,-11.1518 -4.33681,-8.67362 0.61954,-3.71727 1.85864,-6.81499 5.5759,-16.7277 9.91271,-19.82542 6.19544,-8.05408 8.67363,-6.19545 16.7277,-3.09772 10.53225,-1.23909 29.11859,-6.81499 22.92315,-7.43453 19.20588,-6.81499 9.29317,-8.67362 9.29317,-9.91271 5.5759,-5.5759" /> 
			<path onclick="nextTask($(this))" pathID="1" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][3].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 746.55111,556.8777 -19.20587,0.61954 -12.39089,0.61955 -8.05408,-3.71727 -14.24953,-9.29317 -22.3036,-14.86906 -19.82542,-12.39089 -21.68406,-9.91272 -22.92315,-9.29316 -25.40132,-9.29317 -18.58633,-8.05408 -16.10816,-4.95635 -9.91271,-3.71727 -10.53226,2.47818 -13.01043,3.71726 -15.48862,4.33681 -9.29316,0.61955 -6.81499,-1.85863 -6.19545,-3.71727 -8.67362,-14.86907 -8.67362,-17.34724 -10.53226,-22.30361 -5.5759,-11.1518 -4.33681,-8.67362 0.61954,-3.71727 1.85864,-6.81499 5.5759,-16.7277 9.91271,-19.82542 6.19544,-8.05408 8.67363,-6.19545 16.7277,-3.09772 10.53225,-1.23909 29.11859,-6.81499 22.92315,-7.43453 19.20588,-6.81499 9.29317,-8.67362 9.29317,-9.91271 5.5759,-5.5759" /> 
			<path onclick="nextTask($(this))" pathID="2" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][7].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 1162.2655,977.54841 13.0104,-21.06451 9.9127,-7.43454 13.63,-9.91271 10.5323,-4.95635 13.0104,-4.95636 14.8691,-2.47818 11.1518,-3.09772 14.2495,-6.81499 23.5427,-13.01043 16.1081,-11.15181 16.1082,-8.67362 16.7277,-10.53225 26.0209,-9.91272 16.1081,-8.67362 9.9127,-4.33681 7.4346,-0.61955 11.7713,14.24953 25.4013,25.40132 15.4886,16.7277 6.1955,6.19545 2.4782,6.81499 -10.5323,27.25996 -12.3909,23.54269 -10.5322,12.39089 -9.2932,9.91271 -5.5759,6.19544"/>
			<path onclick="nextTask($(this))" pathID="2" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][7].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 1162.2655,977.54841 13.0104,-21.06451 9.9127,-7.43454 13.63,-9.91271 10.5323,-4.95635 13.0104,-4.95636 14.8691,-2.47818 11.1518,-3.09772 14.2495,-6.81499 23.5427,-13.01043 16.1081,-11.15181 16.1082,-8.67362 16.7277,-10.53225 26.0209,-9.91272 16.1081,-8.67362 9.9127,-4.33681 7.4346,-0.61955 11.7713,14.24953 25.4013,25.40132 15.4886,16.7277 6.1955,6.19545 2.4782,6.81499 -10.5323,27.25996 -12.3909,23.54269 -10.5322,12.39089 -9.2932,9.91271 -5.5759,6.19544"/>`;
		}
		
	},
	{
		// 1
		img: "img/14/1.png",
		corRoad: 1,
		objectiveRoad:"Road 3",
		x: "0",
		y: "0",
		paths: function(legendIndex)
		{
			return `<path onclick="nextTask($(this))" pathID="0" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][0].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 1533.9548,971.4044 -80.5502,73.5458 -4.5028,-14.0087 -5.0031,-11.5072 -7.5047,-8.5053 -29.5183,-30.01868 -18.5116,-21.51339 -13.0081,-17.5109 -19.5121,-24.51526 -19.0118,-22.0137 -9.5059,-14.00872 -9.506,-9.00561 -16.0099,-10.50654 -26.0162,-15.00934 -20.5128,-11.00685 -25.5159,29.51837 -29.5183,36.02242 -133.0829,154.0959"/>
			<path onclick="nextTask($(this))" pathID="0" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][0].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 1533.9548,971.4044 -80.5502,73.5458 -4.5028,-14.0087 -5.0031,-11.5072 -7.5047,-8.5053 -29.5183,-30.01868 -18.5116,-21.51339 -13.0081,-17.5109 -19.5121,-24.51526 -19.0118,-22.0137 -9.5059,-14.00872 -9.506,-9.00561 -16.0099,-10.50654 -26.0162,-15.00934 -20.5128,-11.00685 -25.5159,29.51837 -29.5183,36.02242 -133.0829,154.0959"/>
			<path onclick="nextTask($(this))" pathID="1" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][2].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 596.8715,1061.4605 -37.52335,-17.0106 -16.00997,-10.5066 -11.00685,-20.5127 -3.50218,-17.01063 -3.50218,-28.01744 -4.00249,-21.51339 3.50218,-13.50841 2.50156,-19.01183 -2.00125,-30.519 1.00063,-46.52896 4.00249,-12.00747 9.50591,-15.50965 -0.50031,-18.51153 -7.50467,-23.51463 -1.00062,-13.0081 6.00374,-24.01495 4.5028,-33.52086 2.50155,-28.51775 1.50094,-10.00623 4.00249,-7.00436 2.50156,-4.5028 11.00685,-7.50467 18.01121,-4.00249 18.01121,-6.00374 14.00872,-12.50778 17.01059,-19.51215 5.50342,-6.00374 -9.50592,-13.00809 -10.00622,-24.51526"/>
			<path onclick="nextTask($(this))" pathID="1" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][2].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 596.8715,1061.4605 -37.52335,-17.0106 -16.00997,-10.5066 -11.00685,-20.5127 -3.50218,-17.01063 -3.50218,-28.01744 -4.00249,-21.51339 3.50218,-13.50841 2.50156,-19.01183 -2.00125,-30.519 1.00063,-46.52896 4.00249,-12.00747 9.50591,-15.50965 -0.50031,-18.51153 -7.50467,-23.51463 -1.00062,-13.0081 6.00374,-24.01495 4.5028,-33.52086 2.50155,-28.51775 1.50094,-10.00623 4.00249,-7.00436 2.50156,-4.5028 11.00685,-7.50467 18.01121,-4.00249 18.01121,-6.00374 14.00872,-12.50778 17.01059,-19.51215 5.50342,-6.00374 -9.50592,-13.00809 -10.00622,-24.51526"/>
			<path onclick="nextTask($(this))" pathID="2" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][4].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none" d="M 950.9434,307.78302 974.29245,300 1012.5,290.09434 l 19.8113,-16.27359 28.3019,-31.83962 19.1038,-19.81132 12.7358,-16.27359 16.9812,-7.07547 16.9811,-4.95283 13.4434,-1.41509 12.0283,2.12264 14.1509,2.12264 4.2453,1.4151 1.4151,-11.32076 -1.4151,-6.36792 -4.9528,-7.07547 -4.9528,-4.24529 -9.9057,-4.95283 -14.1509,-0.70755 -12.7359,2.83019 -15.566,2.12264 -18.3963,-9.90566 -22.6415,-16.27358 -22.6415,-17.68868 -11.3207,-6.36792 -8.4906,-2.83019 -9.9057,-0.70755 -12.7358,0 -14.15095,-2.83019 -16.27358,-9.90566 L 956.60378,92.688678 949.5283,87.0283 940.33019,84.198112 928.30189,82.783017 916.98113,82.07547 907.07547,80.660376 900,77.12264 l -6.36792,-10.613208 -5.66038,-9.90566 -4.95283,-8.490566 -3.53774,-3.537736 -4.95283,0 -4.95283,0 -2.12264,0.707547"/>
			<path onclick="nextTask($(this))" pathID="2" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][4].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none" d="M 950.9434,307.78302 974.29245,300 1012.5,290.09434 l 19.8113,-16.27359 28.3019,-31.83962 19.1038,-19.81132 12.7358,-16.27359 16.9812,-7.07547 16.9811,-4.95283 13.4434,-1.41509 12.0283,2.12264 14.1509,2.12264 4.2453,1.4151 1.4151,-11.32076 -1.4151,-6.36792 -4.9528,-7.07547 -4.9528,-4.24529 -9.9057,-4.95283 -14.1509,-0.70755 -12.7359,2.83019 -15.566,2.12264 -18.3963,-9.90566 -22.6415,-16.27358 -22.6415,-17.68868 -11.3207,-6.36792 -8.4906,-2.83019 -9.9057,-0.70755 -12.7358,0 -14.15095,-2.83019 -16.27358,-9.90566 L 956.60378,92.688678 949.5283,87.0283 940.33019,84.198112 928.30189,82.783017 916.98113,82.07547 907.07547,80.660376 900,77.12264 l -6.36792,-10.613208 -5.66038,-9.90566 -4.95283,-8.490566 -3.53774,-3.537736 -4.95283,0 -4.95283,0 -2.12264,0.707547"/>`;
		}
	},
	{
		// 2
		img: "img/14/2.png",
		corRoad: 0,
		objectiveRoad:"Road 2",
		x: "0",
		y: "0",
		paths: function(legendIndex)
		{
			return `<path onclick="nextTask($(this))" pathID="0" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][1].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 1317.8202,919.87232 -36.0224,-2.00124 -57.0355,-13.0081 -56.0349,-13.00809 -57.0355,-3.00187 -39.0243,6.00373 -56.0348,16.00997 -52.03242,6.00374 -56.03487,-4.00249 -50.03114,-5.00312 -50.03114,6.00374 -101.06291,30.01868 -103.06415,51.03177"/>
			<path onclick="nextTask($(this))" pathID="0" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][1].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 1317.8202,919.87232 -36.0224,-2.00124 -57.0355,-13.0081 -56.0349,-13.00809 -57.0355,-3.00187 -39.0243,6.00373 -56.0348,16.00997 -52.03242,6.00374 -56.03487,-4.00249 -50.03114,-5.00312 -50.03114,6.00374 -101.06291,30.01868 -103.06415,51.03177"/>
			<path onclick="nextTask($(this))" pathID="1" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][4].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 796.49575,53.332976 -27.01681,31.019307 -34.02118,37.023047 -43.02678,42.02615 -12.00747,9.00561 -20.01246,2.00125 -23.01432,-3.00187 -17.01059,-3.00187 -18.01121,0 -43.02678,2.00124 -66.04111,7.00436 -60.03737,8.00499 -48.02989,13.00809 -63.03924,35.0218 -37.02304,27.01682 -24.01495,29.01806"/>
			<path onclick="nextTask($(this))" pathID="1" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][4].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 796.49575,53.332976 -27.01681,31.019307 -34.02118,37.023047 -43.02678,42.02615 -12.00747,9.00561 -20.01246,2.00125 -23.01432,-3.00187 -17.01059,-3.00187 -18.01121,0 -43.02678,2.00124 -66.04111,7.00436 -60.03737,8.00499 -48.02989,13.00809 -63.03924,35.0218 -37.02304,27.01682 -24.01495,29.01806"/>
			<path onclick="nextTask($(this))" pathID="2" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][2].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 541.98113,492.45283 24.05661,-36.79245 25.47169,-38.20755 19.81133,-16.98113 33.96226,-21.22642 28.30189,-12.73585 22.64151,-5.66037 55.18868,4.24528 33.96226,1.41509 32.54717,1.4151 45.28302,11.32075 52.35849,11.32076 227.83016,55.18868 16.9812,1.41509 22.6415,0"/>
			<path onclick="nextTask($(this))" pathID="2" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][2].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 541.98113,492.45283 24.05661,-36.79245 25.47169,-38.20755 19.81133,-16.98113 33.96226,-21.22642 28.30189,-12.73585 22.64151,-5.66037 55.18868,4.24528 33.96226,1.41509 32.54717,1.4151 45.28302,11.32075 52.35849,11.32076 227.83016,55.18868 16.9812,1.41509 22.6415,0"/>`;
		}
	},
	{
		// 3
		img: "img/14/3.png",
		corRoad: 0,
		objectiveRoad:"Road 6",
		x: "0",
		y: "0",
		paths: function(legendIndex)
		{
			return `<path onclick="nextTask($(this))" pathID="0" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][5].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 814.50696,950.89163 106.06602,-47.02927 121.07532,-64.03986 94.0586,-72.04484 83.0517,-67.04173 54.0336,-41.02553 45.028,-27.01682 24.015,-13.0081"/>
			<path onclick="nextTask($(this))" pathID="0" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][5].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 814.50696,950.89163 106.06602,-47.02927 121.07532,-64.03986 94.0586,-72.04484 83.0517,-67.04173 54.0336,-41.02553 45.028,-27.01682 24.015,-13.0081"/>
			<path onclick="nextTask($(this))" pathID="1" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][6].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 584.36372,636.69607 81.05045,110.06851 35.02179,-39.02429 14.00872,-14.00872 11.00685,-21.01308 26.0162,-121.07536 0,-5.00311 -2.00125,-5.00312 -2.00124,-3.00186 7.00435,-6.00374 4.0025,-12.00747 4.00249,-26.0162 16.00996,-43.02678 0,-25.01557 8.00498,-28.01744 13.0081,-32.01993 11.00685,-19.01183 20.01246,-15.00934"/>
			<path onclick="nextTask($(this))" pathID="1" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][6].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 584.36372,636.69607 81.05045,110.06851 35.02179,-39.02429 14.00872,-14.00872 11.00685,-21.01308 26.0162,-121.07536 0,-5.00311 -2.00125,-5.00312 -2.00124,-3.00186 7.00435,-6.00374 4.0025,-12.00747 4.00249,-26.0162 16.00996,-43.02678 0,-25.01557 8.00498,-28.01744 13.0081,-32.01993 11.00685,-19.01183 20.01246,-15.00934"/>
			<path onclick="nextTask($(this))" pathID="2" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][1].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 1422.8774,300.70755 -6.368,0 -5.6603,0 -16.2736,42.45283 -62.9717,-26.8868 -24.0566,-9.19811 -28.3019,-5.66038 -13.4434,-3.53773 -99.7642,14.85849 -11.3207,4.95283 -6.3679,-2.12264 0.7075,-12.02831 -4.9528,-19.81132 -0.7076,-24.0566 -2.8302,-9.90566 -2.8301,-4.24528 33.9622,-12.73585 22.6415,-10.61321 26.1793,-12.73585 21.9339,-10.61321 13.4434,-4.95283 14.8585,30.42453 38.2076,7.07547 0,0"/>
			<path onclick="nextTask($(this))" pathID="2" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][1].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 1422.8774,300.70755 -6.368,0 -5.6603,0 -16.2736,42.45283 -62.9717,-26.8868 -24.0566,-9.19811 -28.3019,-5.66038 -13.4434,-3.53773 -99.7642,14.85849 -11.3207,4.95283 -6.3679,-2.12264 0.7075,-12.02831 -4.9528,-19.81132 -0.7076,-24.0566 -2.8302,-9.90566 -2.8301,-4.24528 33.9622,-12.73585 22.6415,-10.61321 26.1793,-12.73585 21.9339,-10.61321 13.4434,-4.95283 14.8585,30.42453 38.2076,7.07547 0,0"/>`;
		}
	},
	
	{
		// 4
		img: "img/14/4.png",
		corRoad: 0,
		objectiveRoad:"Road 1",
		x: "0",
		y: "0",
		paths: function(legendIndex)
		{
			return `<path onclick="nextTask($(this))" pathID="0" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][0].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 192.11958,56.334844 26.01619,12.007474 39.02429,0 23.01433,-4.002491 28.01743,5.003114 34.02118,10.006228 32.01993,-2.001246 69.04297,9.005606 38.02367,6.003736 33.02055,21.013075 73.04547,56.03488 14.00872,10.00623 6.00373,-10.00623 11.00685,-6.00374 25.01557,1.00063 0,-20.01246 4.00249,-24.01495 -1.00062,-15.00934 -2.00124,-6.003735 19.01183,-5.003114"/>
			<path onclick="nextTask($(this))" pathID="0" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][0].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 192.11958,56.334844 26.01619,12.007474 39.02429,0 23.01433,-4.002491 28.01743,5.003114 34.02118,10.006228 32.01993,-2.001246 69.04297,9.005606 38.02367,6.003736 33.02055,21.013075 73.04547,56.03488 14.00872,10.00623 6.00373,-10.00623 11.00685,-6.00374 25.01557,1.00063 0,-20.01246 4.00249,-24.01495 -1.00062,-15.00934 -2.00124,-6.003735 19.01183,-5.003114"/>
			<path onclick="nextTask($(this))" pathID="1" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][7].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 123.07661,205.42764 70.04359,12.00748 56.03488,28.01743 74.04609,47.02928 119.07411,75.04671 113.07038,72.04484 81.05044,41.02553 79.04921,17.01059 88.0548,7.00436"/>
			<path onclick="nextTask($(this))" pathID="1" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][7].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 123.07661,205.42764 70.04359,12.00748 56.03488,28.01743 74.04609,47.02928 119.07411,75.04671 113.07038,72.04484 81.05044,41.02553 79.04921,17.01059 88.0548,7.00436"/>
			<path onclick="nextTask($(this))" pathID="2" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][5].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 874.54433,709.74154 -15.00934,8.00498 -52.03239,-10.00623 -42.02615,-7.00436 -36.02243,-7.00436 -27.01681,-8.00498 -33.02055,-14.00872 -31.01931,-13.0081 -12.00747,-1.00062 -17.01059,0 -26.01619,5.00311 -30.01869,9.00561 -44.0274,-3.00187 -35.0218,-6.00374 -35.0218,-3.00186 -31.01931,-2.00125 -21.01307,-4.00249 -16.00997,-7.00436 -10.00623,-15.00934 -39.02429,-66.04111 -17.01058,-20.01245 -34.02118,-13.0081 -36.02242,-11.00685 -16.00997,-4.00249 -30.01868,4.00249 -37.02304,4.00249 -40.02492,-5.00311 -26.016189,-11.00686 -32.01993,-4.00249 -12.007473,-4.00249"/>
			<path onclick="nextTask($(this))" pathID="2" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][5].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 874.54433,709.74154 -15.00934,8.00498 -52.03239,-10.00623 -42.02615,-7.00436 -36.02243,-7.00436 -27.01681,-8.00498 -33.02055,-14.00872 -31.01931,-13.0081 -12.00747,-1.00062 -17.01059,0 -26.01619,5.00311 -30.01869,9.00561 -44.0274,-3.00187 -35.0218,-6.00374 -35.0218,-3.00186 -31.01931,-2.00125 -21.01307,-4.00249 -16.00997,-7.00436 -10.00623,-15.00934 -39.02429,-66.04111 -17.01058,-20.01245 -34.02118,-13.0081 -36.02242,-11.00685 -16.00997,-4.00249 -30.01868,4.00249 -37.02304,4.00249 -40.02492,-5.00311 -26.016189,-11.00686 -32.01993,-4.00249 -12.007473,-4.00249"/>`;
		}
	},
	{
		// 5
		img: "img/14/5.png",
		corRoad: 1,
		objectiveRoad:"Road 2",
		x: "0",
		y: "0",
		paths: function(legendIndex)
		{
			return `<path onclick="nextTask($(this))" pathID="0" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][3].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 791.74528,536.32075 29.71699,99.05661 19.10377,63.67924 21.93396,49.52831 19.10377,19.81132 19.10378,9.90566 31.83962,5.66037 38.20755,4.95283 29.00943,13.4434 14.85845,8.49057 21.2265,26.17924 31.132,31.83962 26.1793,28.30189 19.8113,24.0566 4.2453,14.8585"/>
			<path onclick="nextTask($(this))" pathID="0" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][3].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 791.74528,536.32075 29.71699,99.05661 19.10377,63.67924 21.93396,49.52831 19.10377,19.81132 19.10378,9.90566 31.83962,5.66037 38.20755,4.95283 29.00943,13.4434 14.85845,8.49057 21.2265,26.17924 31.132,31.83962 26.1793,28.30189 19.8113,24.0566 4.2453,14.8585"/>
			<path onclick="nextTask($(this))" pathID="1" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][1].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 1172.0519,358.72641 -14.5047,3.18397 -53.0661,-11.32076 -34.316,7.07547 -3.8915,-8.49056 -7.4293,-12.38208 -2.4764,-4.24528 -6.7217,-2.83019 -4.599,-7.78302 -12.7359,-26.53302 -44.22168,8.49057 6.36793,-10.96698 2.47641,-13.4434 1.76887,-21.93396 1.76887,-12.38208 1.4151,-4.95283 5.6604,-9.55188 9.1981,-12.73585 7.783,-5.66038 14.1509,-1.06132 14.5048,-12.73585 9.1981,-12.73585 -0.3538,-6.36792 -7.0755,-14.50472 6.0142,-22.28774 5.6604,-15.21226 0,-10.25944 8.8443,-1.41509 6.3679,-4.24528 6.3679,-4.24529 6.7217,0 5.3067,-7.783015 0,0 0,-0.353773"/>
			<path onclick="nextTask($(this))" pathID="1" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][1].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 1172.0519,358.72641 -14.5047,3.18397 -53.0661,-11.32076 -34.316,7.07547 -3.8915,-8.49056 -7.4293,-12.38208 -2.4764,-4.24528 -6.7217,-2.83019 -4.599,-7.78302 -12.7359,-26.53302 -44.22168,8.49057 6.36793,-10.96698 2.47641,-13.4434 1.76887,-21.93396 1.76887,-12.38208 1.4151,-4.95283 5.6604,-9.55188 9.1981,-12.73585 7.783,-5.66038 14.1509,-1.06132 14.5048,-12.73585 9.1981,-12.73585 -0.3538,-6.36792 -7.0755,-14.50472 6.0142,-22.28774 5.6604,-15.21226 0,-10.25944 8.8443,-1.41509 6.3679,-4.24528 6.3679,-4.24529 6.7217,0 5.3067,-7.783015 0,0 0,-0.353773"/>
			<path onclick="nextTask($(this))" pathID="2" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][4].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 564.35126,314.49553 -4.00249,-8.00498 -0.50031,-7.50468 0.50031,-5.50342 -28.01744,-31.01931 -14.50903,-17.01059 -12.50778,-16.51027 -7.50467,-13.50841 -8.5053,-6.50405 -12.00747,-3.00187 -20.01246,2.00125 -16.00996,-1.50094 -97.06041,-25.51588 -12.00748,-7.50467 -8.50529,-11.50716 -7.00436,-9.50592 -4.5028,-6.00373 -7.50468,-4.50281 -38.02366,-11.50716 -22.51402,-9.50592 -10.50653,-3.50218 -4.50281,-0.50031 -18.51152,8.5053 -13.0081,8.50529 -17.01058,8.5053 -19.51215,8.00498 0,0.50031"/>
			<path onclick="nextTask($(this))" pathID="2" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][4].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 564.35126,314.49553 -4.00249,-8.00498 -0.50031,-7.50468 0.50031,-5.50342 -28.01744,-31.01931 -14.50903,-17.01059 -12.50778,-16.51027 -7.50467,-13.50841 -8.5053,-6.50405 -12.00747,-3.00187 -20.01246,2.00125 -16.00996,-1.50094 -97.06041,-25.51588 -12.00748,-7.50467 -8.50529,-11.50716 -7.00436,-9.50592 -4.5028,-6.00373 -7.50468,-4.50281 -38.02366,-11.50716 -22.51402,-9.50592 -10.50653,-3.50218 -4.50281,-0.50031 -18.51152,8.5053 -13.0081,8.50529 -17.01058,8.5053 -19.51215,8.00498 0,0.50031"/>`;
		}
	},
	{
		// 6
		img: "img/15/0.png",
		corRoad: 1,
		objectiveRoad:"Road 4",
		x: "0",
		y: "0",
		paths: function(legendIndex)
		{
			return `<path onclick="nextTask($(this))" pathID="0" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][6].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 1186.7386,794.79447 -27.0168,40.02492 -16.0099,15.00934 -24.015,26.01619 -30.0187,35.0218 -6.0037,1.00062 -5.0031,4.00249 -3.0019,6.00374 -31.0193,9.00561 -101.06289,17.01058 -15.00935,2.00125 -48.02989,-16.00997 -33.02055,-21.01307 -8.00499,-10.00623 1.00063,-6.00374 -2.00125,-5.00311 21.01308,-12.00748 24.01495,-25.01557 9.0056,-15.00934 3.00187,-17.01059 -4.00249,-22.0137 -6.00374,-32.01993 0,-25.01557 6.00374,-17.01059 12.00747,-15.00934 19.01184,-20.01245 18.01121,-16.00997 7.00436,-6.00374 12.00747,-5.00311"/>
			<path onclick="nextTask($(this))" pathID="0" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][6].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 1186.7386,794.79447 -27.0168,40.02492 -16.0099,15.00934 -24.015,26.01619 -30.0187,35.0218 -6.0037,1.00062 -5.0031,4.00249 -3.0019,6.00374 -31.0193,9.00561 -101.06289,17.01058 -15.00935,2.00125 -48.02989,-16.00997 -33.02055,-21.01307 -8.00499,-10.00623 1.00063,-6.00374 -2.00125,-5.00311 21.01308,-12.00748 24.01495,-25.01557 9.0056,-15.00934 3.00187,-17.01059 -4.00249,-22.0137 -6.00374,-32.01993 0,-25.01557 6.00374,-17.01059 12.00747,-15.00934 19.01184,-20.01245 18.01121,-16.00997 7.00436,-6.00374 12.00747,-5.00311"/>
			<path onclick="nextTask($(this))" pathID="1" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][3].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 324.20179,617.68424 34.02117,51.03176 40.02492,22.0137 29.01806,-31.0193 9.0056,-15.00935 16.00997,-9.0056 22.0137,-8.00498 6.00374,-2.00125 -4.00249,-12.00747 -12.00748,-20.01246 -10.00623,-25.01557 -2.00124,-41.02553 6.00373,-17.01059 10.00623,-13.0081 23.01433,-10.00623 17.01058,-11.00685 9.00561,-9.0056 11.00685,-3.00187 1.00062,-5.00312 12.00748,-20.01245 15.00934,-20.01246 20.01246,7.00436 32.01993,-12.00747 15.00934,-15.00934 4.00249,-5.00312 9.0056,-3.00187 9.00561,-16.00996 1.00062,-9.00561 -4.00249,-11.00685 6.00374,-11.00685 0,0"/>
			<path onclick="nextTask($(this))" pathID="1" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][3].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 324.20179,617.68424 34.02117,51.03176 40.02492,22.0137 29.01806,-31.0193 9.0056,-15.00935 16.00997,-9.0056 22.0137,-8.00498 6.00374,-2.00125 -4.00249,-12.00747 -12.00748,-20.01246 -10.00623,-25.01557 -2.00124,-41.02553 6.00373,-17.01059 10.00623,-13.0081 23.01433,-10.00623 17.01058,-11.00685 9.00561,-9.0056 11.00685,-3.00187 1.00062,-5.00312 12.00748,-20.01245 15.00934,-20.01246 20.01246,7.00436 32.01993,-12.00747 15.00934,-15.00934 4.00249,-5.00312 9.0056,-3.00187 9.00561,-16.00996 1.00062,-9.00561 -4.00249,-11.00685 6.00374,-11.00685 0,0"/>
			<path onclick="nextTask($(this))" pathID="2" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][0].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 1095.682,434.57026 18.0112,-70.04359 43.0268,34.02117 10.0062,-14.00872 21.0131,-29.01806 75.0467,-148.09217 -48.0299,-44.02741 -57.0355,-39.02428 -61.038,117.07286 -16.01,-3.00187 -4.0025,13.0081 -6.0037,10.00623 -6.0037,10.00623 -1.0007,9.0056 -108.06722,-15.00934 -29.01806,88.05481"/>
			<path onclick="nextTask($(this))" pathID="2" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][0].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 1095.682,434.57026 18.0112,-70.04359 43.0268,34.02117 10.0062,-14.00872 21.0131,-29.01806 75.0467,-148.09217 -48.0299,-44.02741 -57.0355,-39.02428 -61.038,117.07286 -16.01,-3.00187 -4.0025,13.0081 -6.0037,10.00623 -6.0037,10.00623 -1.0007,9.0056 -108.06722,-15.00934 -29.01806,88.05481"/>`;	
		}
	},
	{
		// 7
		img: "img/15/1.png",
		corRoad: 0,
		objectiveRoad:"Road 1",
		x: "0",
		y: "0",
		paths: function(legendIndex)
		{
			return `<path onclick="nextTask($(this))" pathID="0" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][0].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 1210.7536,923.87482 -21.0131,-27.01682 -18.0112,-12.00747 -8.005,-1.00063 -18.0112,0 -22.0137,9.00561 -15.0093,11.00685 -14.0088,18.01121 -24.0149,32.01993 -24.0149,30.01868 -21.0131,17.01062 -39.02431,6.0037 -50.03114,-2.0012 -40.02492,-1.0007 -51.03176,-3.0018 -61.03799,-1.00065 -33.02055,0 -38.02367,4.00245 -23.01432,10.0063 -20.01246,15.0093 -21.01308,23.0143 -26.01619,20.0125 -24.01495,15.0093"/>
			<path onclick="nextTask($(this))" pathID="0" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][0].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 1210.7536,923.87482 -21.0131,-27.01682 -18.0112,-12.00747 -8.005,-1.00063 -18.0112,0 -22.0137,9.00561 -15.0093,11.00685 -14.0088,18.01121 -24.0149,32.01993 -24.0149,30.01868 -21.0131,17.01062 -39.02431,6.0037 -50.03114,-2.0012 -40.02492,-1.0007 -51.03176,-3.0018 -61.03799,-1.00065 -33.02055,0 -38.02367,4.00245 -23.01432,10.0063 -20.01246,15.0093 -21.01308,23.0143 -26.01619,20.0125 -24.01495,15.0093"/>
			<path onclick="nextTask($(this))" pathID="1" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][1].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 874.54433,469.59206 -12.00747,17.01059 -8.00498,12.00747 0,13.0081 8.00498,15.00934 12.00747,12.00748 17.01059,8.00498 18.01121,9.0056 7.00436,18.01121 -4.00249,13.0081 -14.00872,28.01744 -18.01121,18.01121 -18.01121,15.00934 -19.01184,9.00561 -76.04733,1.00062 -8.00498,-152.09467 3.00187,-35.02179 12.00747,-32.01993 17.01059,-24.01495 19.01183,-16.00997 27.01682,-13.00809 36.02242,-11.00685 125.07785,-40.02492" />
			<path onclick="nextTask($(this))" pathID="1" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][1].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 874.54433,469.59206 -12.00747,17.01059 -8.00498,12.00747 0,13.0081 8.00498,15.00934 12.00747,12.00748 17.01059,8.00498 18.01121,9.0056 7.00436,18.01121 -4.00249,13.0081 -14.00872,28.01744 -18.01121,18.01121 -18.01121,15.00934 -19.01184,9.00561 -76.04733,1.00062 -8.00498,-152.09467 3.00187,-35.02179 12.00747,-32.01993 17.01059,-24.01495 19.01183,-16.00997 27.01682,-13.00809 36.02242,-11.00685 125.07785,-40.02492" />
			<path onclick="nextTask($(this))" pathID="2" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][2].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 1184.434,639.62264 66.5094,28.30189 59.434,1.41509 50.9434,-5.66038 18.3962,-9.90566 22.6415,-18.39622 12.7358,-28.30189 11.3208,-24.0566 -60.8491,-29.71698 -19.8113,-12.73585 L 1335.8491,525 1334.434,509.43396 1350,427.35849 l 1.4151,-31.13208 -2.8302,-59.43396 L 1328.7736,300"/>
			<path onclick="nextTask($(this))" pathID="2" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][2].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 1184.434,639.62264 66.5094,28.30189 59.434,1.41509 50.9434,-5.66038 18.3962,-9.90566 22.6415,-18.39622 12.7358,-28.30189 11.3208,-24.0566 -60.8491,-29.71698 -19.8113,-12.73585 L 1335.8491,525 1334.434,509.43396 1350,427.35849 l 1.4151,-31.13208 -2.8302,-59.43396 L 1328.7736,300"/>`;
		}
	},
	{
		// 8
		img: "img/15/2.png",
		corRoad: 2,
		objectiveRoad:"Road 8",
		x: "0",
		y: "0",
		paths: function(legendIndex)
		{
			return `<path onclick="nextTask($(this))" pathID="0" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][5].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="M 876.03591,1037.0247 890.90498,832.575 877.275,802.83686 l -30.97722,-17.34724 -45.8463,-22.3036 -52.04173,-7.43454 -8.67363,0 -55.759,2.47818 -33.4554,4.95636 -16.10816,4.95635 -45.84629,28.49905 -30.97723,23.54269 -16.10815,-12.39089 -8.67363,-3.71727 -2.47817,-21.06451 0,0"/>
			<path onclick="nextTask($(this))" pathID="0" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][5].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="M 876.03591,1037.0247 890.90498,832.575 877.275,802.83686 l -30.97722,-17.34724 -45.8463,-22.3036 -52.04173,-7.43454 -8.67363,0 -55.759,2.47818 -33.4554,4.95636 -16.10816,4.95635 -45.84629,28.49905 -30.97723,23.54269 -16.10815,-12.39089 -8.67363,-3.71727 -2.47817,-21.06451 0,0"/>
			<path onclick="nextTask($(this))" pathID="1" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][6].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 1135.0055,791.68506 0,-11.1518 -35.9336,-125.14799 -11.1518,-40.88993 -2.4781,-23.54269 1.239,-50.80265 -13.6299,-24.78178 1.2391,-4.95636 2.4781,-49.56356 8.6737,-13.62998 16.1081,-9.91271 -12.3909,-14.86907 -35.9336,-4.95635 -4.9563,-3.71727 3.7172,1.23909" />
			<path onclick="nextTask($(this))" pathID="1" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][6].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 1135.0055,791.68506 0,-11.1518 -35.9336,-125.14799 -11.1518,-40.88993 -2.4781,-23.54269 1.239,-50.80265 -13.6299,-24.78178 1.2391,-4.95636 2.4781,-49.56356 8.6737,-13.62998 16.1081,-9.91271 -12.3909,-14.86907 -35.9336,-4.95635 -4.9563,-3.71727 3.7172,1.23909" />
			<path onclick="nextTask($(this))" pathID="2" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][7].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 758.32246,318.35307 -6.19545,18.58633 -23.54269,18.58634 -26.02086,12.39089 -37.17267,11.1518 -19.82543,8.67362 -3.71727,4.95636 0,6.19544 6.19545,12.39089 -40.88994,13.62998 -34.69449,13.62998 -79.30169,59.47627 -61.95445,53.28083 -7.43454,-1.23909 -16.10815,1.23909 -26.02087,6.19544 -17.34725,1.23909 -3.71727,-2.47817 -1.23908,-9.91272 4.95635,-16.10815 11.1518,1.23909 17.34725,0 21.06451,-4.95636"/>
			<path onclick="nextTask($(this))" pathID="2" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][7].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 758.32246,318.35307 -6.19545,18.58633 -23.54269,18.58634 -26.02086,12.39089 -37.17267,11.1518 -19.82543,8.67362 -3.71727,4.95636 0,6.19544 6.19545,12.39089 -40.88994,13.62998 -34.69449,13.62998 -79.30169,59.47627 -61.95445,53.28083 -7.43454,-1.23909 -16.10815,1.23909 -26.02087,6.19544 -17.34725,1.23909 -3.71727,-2.47817 -1.23908,-9.91272 4.95635,-16.10815 11.1518,1.23909 17.34725,0 21.06451,-4.95636"/>`;	
		}
	},
	{
		// 9
		img: "img/15/3.png",
		corRoad: 2,
		objectiveRoad:"Road 8",
		x: "0",
		y: "0",
		paths: function(legendIndex)
		{
			return `<path onclick="nextTask($(this))" pathID="0" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][3].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 1112.2642,721.69811 -25.4717,4.24529 -11.3208,-4.24529 -65.0943,-39.62264 -24.05664,-25.4717 -49.52831,-69.33962 -70.75471,-42.45283 -127.35849,-63.67925 38.20754,-24.0566 56.60378,-14.15094 22.64151,-1.4151"/>
			<path onclick="nextTask($(this))" pathID="0" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][3].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 1112.2642,721.69811 -25.4717,4.24529 -11.3208,-4.24529 -65.0943,-39.62264 -24.05664,-25.4717 -49.52831,-69.33962 -70.75471,-42.45283 -127.35849,-63.67925 38.20754,-24.0566 56.60378,-14.15094 22.64151,-1.4151"/>
			<path onclick="nextTask($(this))" pathID="1" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][4].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 1030.1887,1030.1887 -24.0566,-49.52832 -11.32078,-7.07547 -35.37736,-9.90566 -15.56603,-11.32076 -16.98114,-15.56604 -14.15094,-18.39622 -15.56604,-24.05661 -9.90566,-9.90566 -11.32075,-4.24528 -16.98113,-1.4151 -41.03774,0 -9.90566,26.8868 1.41509,21.22641 -4.24528,8.49057 -8.49057,5.66038 -9.90566,-4.24529 -5.66037,-4.24528 -7.07548,-24.0566 -15.56603,-19.81132 -5.66038,-15.56604 5.66038,-21.22642 -19.81132,0 -21.22642,-29.71698 0,0"/>
			<path onclick="nextTask($(this))" pathID="1" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][4].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 1030.1887,1030.1887 -24.0566,-49.52832 -11.32078,-7.07547 -35.37736,-9.90566 -15.56603,-11.32076 -16.98114,-15.56604 -14.15094,-18.39622 -15.56604,-24.05661 -9.90566,-9.90566 -11.32075,-4.24528 -16.98113,-1.4151 -41.03774,0 -9.90566,26.8868 1.41509,21.22641 -4.24528,8.49057 -8.49057,5.66038 -9.90566,-4.24529 -5.66037,-4.24528 -7.07548,-24.0566 -15.56603,-19.81132 -5.66038,-15.56604 5.66038,-21.22642 -19.81132,0 -21.22642,-29.71698 0,0"/>
			<path onclick="nextTask($(this))" pathID="2" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][7].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 1359.9057,544.81132 -93.3963,-60.84906 -32.5471,-16.98113 -39.6227,-12.73585 8.4906,-49.5283 -1.4151,-32.54717 -11.3208,-41.03774 -11.3207,-21.22641 -12.7359,-38.20755 -43.8679,21.22642 -5.6604,-22.64151 0,-9.90566 11.3208,-24.05661 36.7924,-59.43396 0,0"/>
			<path onclick="nextTask($(this))" pathID="2" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][7].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 1359.9057,544.81132 -93.3963,-60.84906 -32.5471,-16.98113 -39.6227,-12.73585 8.4906,-49.5283 -1.4151,-32.54717 -11.3208,-41.03774 -11.3207,-21.22641 -12.7359,-38.20755 -43.8679,21.22642 -5.6604,-22.64151 0,-9.90566 11.3208,-24.05661 36.7924,-59.43396 0,0"/>`;	
		}
	},
	{
		// 10
		img: "img/15/4.png",
		corRoad: 1,
		objectiveRoad:"Road 3",
		x: "0",
		y: "0",
		paths: function(legendIndex)
		{
			return `<path onclick="nextTask($(this))" pathID="0" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][6].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 526.3276,625.68922 -11.00686,-41.02553 18.01122,-8.00499 14.00871,-15.00934 7.00436,-18.01121 1.00063,-47.02927 4.00249,-18.01121 7.00436,-16.00997 12.00747,-15.00934 9.00561,-15.00934 1.00062,-7.00436 27.01682,14.00872 18.01121,15.00934 31.0193,11.00685 42.02616,19.01183 -9.0056,37.02305 -11.00686,24.01495 2.00125,11.00685 19.01183,45.02802 10.00623,1.00063"/>
			<path onclick="nextTask($(this))" pathID="0" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][6].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 526.3276,625.68922 -11.00686,-41.02553 18.01122,-8.00499 14.00871,-15.00934 7.00436,-18.01121 1.00063,-47.02927 4.00249,-18.01121 7.00436,-16.00997 12.00747,-15.00934 9.00561,-15.00934 1.00062,-7.00436 27.01682,14.00872 18.01121,15.00934 31.0193,11.00685 42.02616,19.01183 -9.0056,37.02305 -11.00686,24.01495 2.00125,11.00685 19.01183,45.02802 10.00623,1.00063"/>
			<path onclick="nextTask($(this))" pathID="1" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][2].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 784.66981,1045.7547 14.85849,-7.0755 1.4151,-4.9528 4.24528,0 0,-16.2736 -23.34906,-84.19808 0.70755,-43.16038 12.73585,-2.83019 12.73585,-7.78302 6.36792,-5.66038 19.10378,-33.25471 19.10377,-29.00944 6.36793,-7.78302 6.36792,-3.53773 12.73585,2.83019 14.85849,-0.70755 14.15094,-8.49057 14.15095,-7.78301 11.32075,0 14.15094,5.66037 9.19812,2.83019 14.85849,0 4.95283,4.95283 4.95283,-2.12264 2.12264,-3.53774 33.25468,-10.6132 11.3208,-4.24529 7.0755,-9.19811 9.9056,0"/>
			<path onclick="nextTask($(this))" pathID="1" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][2].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 784.66981,1045.7547 14.85849,-7.0755 1.4151,-4.9528 4.24528,0 0,-16.2736 -23.34906,-84.19808 0.70755,-43.16038 12.73585,-2.83019 12.73585,-7.78302 6.36792,-5.66038 19.10378,-33.25471 19.10377,-29.00944 6.36793,-7.78302 6.36792,-3.53773 12.73585,2.83019 14.85849,-0.70755 14.15094,-8.49057 14.15095,-7.78301 11.32075,0 14.15094,5.66037 9.19812,2.83019 14.85849,0 4.95283,4.95283 4.95283,-2.12264 2.12264,-3.53774 33.25468,-10.6132 11.3208,-4.24529 7.0755,-9.19811 9.9056,0"/>
			<path onclick="nextTask($(this))" pathID="2" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][1].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 1001.6234,503.61324 -9.00558,-46.02865 -9.0056,-39.02429 -4.00249,-36.02242 21.01307,-1.00062 12.0075,-5.00312 17.0106,-2.00124 1.0006,-49.03052 -5.0031,-14.00872 -1.0006,-8.00498 0,-9.00561 4.0024,-7.00436 8.005,-5.00311 8.005,-1.00062 4.0025,-45.02803 4.0025,-30.01868 10.0062,-24.01495 7.0044,-17.01059 3.0018,-11.00685 20.0125,2.00124 19.0118,4.0025 1.0007,27.01681"/>
			<path onclick="nextTask($(this))" pathID="2" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][1].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 1001.6234,503.61324 -9.00558,-46.02865 -9.0056,-39.02429 -4.00249,-36.02242 21.01307,-1.00062 12.0075,-5.00312 17.0106,-2.00124 1.0006,-49.03052 -5.0031,-14.00872 -1.0006,-8.00498 0,-9.00561 4.0024,-7.00436 8.005,-5.00311 8.005,-1.00062 4.0025,-45.02803 4.0025,-30.01868 10.0062,-24.01495 7.0044,-17.01059 3.0018,-11.00685 20.0125,2.00124 19.0118,4.0025 1.0007,27.01681"/>`;	
		}		
	},
	{
		// 11
		img: "img/15/5.png",
		corRoad: 1,
		objectiveRoad:"Road 4",
		x: "0",
		y: "0",
		paths: function(legendIndex)
		{
			return `<path onclick="nextTask($(this))" pathID="0" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][0].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 811.50509,457.58459 2.00125,-71.04422 6.00374,-21.01308 4.00249,-74.04609 75.04671,-8.00498 0,-9.0056 32.01993,-3.00187 25.01557,7.00436 18.01121,15.00934 16.00996,32.01993 25.01555,53.03301 32.02,49.03051 20.0124,15.00935 22.0137,27.01681 36.0224,63.03924 22.0137,-14.00872 3.0019,-11.00685 -25.0156,-37.02304"/>
			<path onclick="nextTask($(this))" pathID="0" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][0].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 811.50509,457.58459 2.00125,-71.04422 6.00374,-21.01308 4.00249,-74.04609 75.04671,-8.00498 0,-9.0056 32.01993,-3.00187 25.01557,7.00436 18.01121,15.00934 16.00996,32.01993 25.01555,53.03301 32.02,49.03051 20.0124,15.00935 22.0137,27.01681 36.0224,63.03924 22.0137,-14.00872 3.0019,-11.00685 -25.0156,-37.02304"/>
				<path onclick="nextTask($(this))" pathID="1" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][3].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 1003.6247,895.85738 -4.00252,-54.03363 0,-23.01433 50.03112,-35.0218 -6.0037,-48.02989 -1.0006,-24.01495 12.0074,-2.00124 35.0218,16.00996 41.0256,19.01183 42.0261,9.00561 45.0281,-4.00249 88.0548,-36.02242 106.066,-48.0299 -16.01,-34.02117" />
				<path onclick="nextTask($(this))" pathID="1" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][3].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 1003.6247,895.85738 -4.00252,-54.03363 0,-23.01433 50.03112,-35.0218 -6.0037,-48.02989 -1.0006,-24.01495 12.0074,-2.00124 35.0218,16.00996 41.0256,19.01183 42.0261,9.00561 45.0281,-4.00249 88.0548,-36.02242 106.066,-48.0299 -16.01,-34.02117" />
			<path onclick="nextTask($(this))" pathID="2" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][7].color};stroke-width: ${STROKE_WIDTH};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"d="m 794.49451,628.69109 -49.03052,4.00249 2.00125,49.03052 -24.01495,10.00623 -34.02118,7.00436 -26.01619,0 -12.00747,0 -4.0025,-4.0025 -4.00249,0 -3.00186,2.00125 -21.01308,0 -1.00063,32.01993 9.00561,105.06539 4.00249,45.02803 8.00498,24.01495 11.00685,30.01868 15.00935,34.02118 7.00435,39.0243 -19.01183,0 -10.00623,-7.00437 -5.00311,-9.00561 -7.00436,-31.01931"/>	
			<path onclick="nextTask($(this))" pathID="2" style="fill:none;fill-rule:evenodd;stroke:${legend[legendIndex][7].color};stroke-width: ${STROKE_WIDTH_GHOST};stroke-linecap:round;stroke-linejoin:round;stroke-opacity:0;stroke-miterlimit:4;stroke-dasharray:none"d="m 794.49451,628.69109 -49.03052,4.00249 2.00125,49.03052 -24.01495,10.00623 -34.02118,7.00436 -26.01619,0 -12.00747,0 -4.0025,-4.0025 -4.00249,0 -3.00186,2.00125 -21.01308,0 -1.00063,32.01993 9.00561,105.06539 4.00249,45.02803 8.00498,24.01495 11.00685,30.01868 15.00935,34.02118 7.00435,39.0243 -19.01183,0 -10.00623,-7.00437 -5.00311,-9.00561 -7.00436,-31.01931"/>`;	
		}
	}
	
];