/**
 * Thank you stack overflow man
 * @param {*} s 
 */
function parseSVG(s)
{
  var div= document.createElementNS('http://www.w3.org/1999/xhtml', 'div');
  div.innerHTML= '<svg xmlns="http://www.w3.org/2000/svg">'+s+'</svg>';
  var frag= document.createDocumentFragment();
  while (div.firstChild.firstChild)
      frag.appendChild(div.firstChild.firstChild);
  return frag;
}

/**
 * Capitalize first letter
 * @param {*string} string 
 * @return {*string}
 */
function capitalizeFirstLetter(string) 
{
   return string.charAt(0).toUpperCase() + string.slice(1);
}

/**
 * Center HTML element in window
 * @param {*HTML element} elem 
 */
function centerElement(elem)
{
	elem.css({
			'top' : $(window).height() / 2 - elem.outerHeight(true) / 2,
			'left' : $(window).width() / 2 - elem.outerWidth(true) / 2
		});
}