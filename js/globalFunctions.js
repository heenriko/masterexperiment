'use strict';
/**
 * 
 */
function initExperiment()
{
	// Set color vision as string:
	let colorVision = $('#formColorVision').val();
	let age = $('#formAge').val();
	let gender = $('#formGender').val();
	
	$('#objectiveRoad').text(tasks[taskIndex].objectiveRoad);

	// Create instance of Observer;
	observer = new Observer(colorVision, age, gender);

	// Get color vision index to determine which palette to use:	
	switch(colorVision)
	{
		case 'protanope': 			daltIndex = 1; break;
		case 'protanomalous': 	daltIndex = 2; break;
		case 'deuteranope': 		daltIndex = 3; break;
		case 'deuteranomalous': daltIndex = 4; break;
		defualt: 								daltIndex = 0; break;	// Not Cvd, tritranopia or tritranomalous.
	}
}
/**
 * 
 */
function startTest()
{
	start = performance.now();	// Start recording;
}
/**
 * 
 */
function finishTest()
{
	stop = performance.now();
	time = parseInt(stop - start) / 1000;
	
	return parseInt(time);
}


/*-----------------------------------------------------------------------------
		S E T   U P   T A S K   D E T A I L S
-----------------------------------------------------------------------------*/

/**
 * Generate legend colors:
 * @param {*} palette 
 */
function generateLegend(palIndex)
{
	let ul = $('.legend ul');
	let lis = "";

	ul.empty();

	for(let i = 0; i < legend[palIndex].length; i++)
	{
		lis += `<li><div class="name liColumn">${ legend[palIndex][i].name }</div>
						<div style="background: ${ legend[palIndex][i].color }" class="color liColumn"></div></li>`;
	}
	ul.append(lis);
}
/**
 * 
 * @param {*} taskIndex 
 */
function setTask(taskIndex, palette)
{
	$('svg path').remove();

	$('svg image').attr('xlink:href',tasks[taskIndex].img);

	$('svg image').attr('x',tasks[taskIndex].x);
	$('svg image').attr('y',tasks[taskIndex].y);
	
	let paths = tasks[taskIndex].paths(palette);

	$('svg').append(parseSVG(paths));

	$('#objectiveRoad').text(tasks[taskIndex].objectiveRoad);
}

/*-----------------------------------------------------------------------------
		N E X T   T A S K
-----------------------------------------------------------------------------*/

/**
 * Set up next task and record response.
 * @param {*} path Clicked on path from the svg.
 */
function nextTask(path)
{
	console.log('taskIndex ' + taskIndex);
	let pathID = parseInt(path.attr('pathID'));
	let pts;
	
	// console.log(pathID);
	// console.log('start ' + start + '\n');
	// console.log('stop ' + stop + '\n');

	if(lastQuestion)
	{
		// Stop time:
		time = finishTest();

		// Determine if correct answer:
		pts = (tasks[taskIndex].corRoad == pathID) ? 1 : 0;   

		// Add task to observer.
		observer.addTask(pathID, time, tasks[taskIndex].corRoad, pts);

		// Remove experiment and show thank you note:
		$('body').css('background','#333');
		$('#objective, #masterContainer').css('display','none');
		$('#results').css('display','block');
		console.log(observer);

		observer.storeObserver();

		observer.createTable();

	}
	else
	{
		//$('svg').css('opacity','0');	// For the break.
		$('#overlay').css('display','block');

		// Stop time:
		time = finishTest();

		// Determine if correct answer:
		pts = (tasks[taskIndex].corRoad == pathID) ? 1 : 0; 

		// Add task to observer.
		observer.addTask(pathID, time, tasks[taskIndex].corRoad, pts);

		//console.log(observer);

		// set up next task:
		taskIndex++;

		// Set time ut between task:
		setTimeout(function()
		{
			// Set up new task:
			if(taskIndex > 11)
			{
				generateLegend(daltIndex);
				setTask(taskIndex, daltIndex);
			}
			else
			{
				generateLegend(0);
				setTask(taskIndex, 0);
			}
			
			//$('svg').css('opacity','1');
			$('#overlay').css('display','none');

			if(taskIndex == tasks.length-1)
			{
				lastQuestion = true;
				startTest();
			}
			else
				// Take the time:
				startTest();	
		},PAUSE);	
	}
}


