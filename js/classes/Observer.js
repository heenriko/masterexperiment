'use strict';

let Observer = function(vision,age,gender)
{
  /* --- Properties --- */
  this.vision = vision;
  this.age = age;
  this.gender = gender;
  this.tasks = []; // Array of observer tasks

  /* --- Methods --- */

  /**
   * Add observer task
   * Push arguments as object into tasks array
   * @param {int} pathID
   * @param {int} time
   * @param {int} correct
   * @param {int} pts
   */
  this.addTask = function(pathID, time, correct, pts)
  {
    this.tasks.push( { "answer": pathID, "time": time, 'correct': correct, "points" : pts } );
  }
  /**
   * 
   */
  this.storeObserver = function()
  {
    $.ajax
    ({
      url: 'include/php/CRUD/create/observer.php',
      type: 'POST',
      data: {'vision': this.vision, 'age': this.age, 'gender': this.gender, 'tasks': JSON.stringify(this.tasks) }
    })
    .done(function(data)
    {
      console.log(data);
    });

  }
  /**
   * 
   */
  this.displayScore = function()
  {
    let score = 0;

    for(let i = 0; i < this.tasks.length; i++)
    {
      score += this.tasks[i].points;
    }

    let formatScore = (score/samples.length*100).toFixed(2); 

    $('#points').html('<span>you scored</span><br>' + formatScore + ' %');
    console.log(this.tasks);
    
  }
  /**
   * 
   */
  this.createTable = function()
  {
    let table = $('#results table');
  
    let tString = "";
    
    tString += `<tr><td>${this.vision}</td><td>${this.age}</td><td>${this.gender}</td>`;

    for(let i = 0; i < this.tasks.length; i++)
    {                // TaskAnswer:                taskTime:              
      tString += '<td>'+this.tasks[i].answer+'</td><td>'+ this.tasks[i].time +'</td>';
    }
    
    tString += '</tr>';

    table.append(tString);
    $('#results').css('display','block');
    $('#mainTest').css('display','none');
  }
}