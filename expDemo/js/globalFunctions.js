// Global functions

function startTest()
{
	
	start = performance.now();	// Start recording;
}

function finishTest()
{
	stop = performance.now();
	time = parseInt(stop - start) / 1000;
	
	return parseInt(time);
}

function capitalizeFirstLetter(string) 
{
   return string.charAt(0).toUpperCase() + string.slice(1);
}

function setScore()
{
	var score = 0;

	for(var i = 0; i < userResp.length; i++)
	{
		score += userResp[i].points;
	}

	$('#points').html('<span>you scored</span><br>' + score/16*100  + ' %');
}

function generateLegend(index)
{
	var type = legendColors[index].type;

	var legendUl = $('#legend ul');
	var items = "";
	var colorAlgIndex = 0;

	switch(type)
	{
		case "org": colorAlgIndex = 0; break;
		case "fid": colorAlgIndex = 1; break;
		case "chr": colorAlgIndex = 2; break;
		case "vis": colorAlgIndex = 3; break;
	}
	
	for(var i = 0; i < colorAlg[colorAlgIndex].legend.length; i++)
	{
		items += '<li><span class = "color" style = "background:' + colorAlg[colorAlgIndex].legend[i].color + '"></span>' + colorAlg[colorAlgIndex].legend[i].cName + '</li>';
	}

	/*for(var i = 0; i < legendColors[index].legend.length; i++)
	{
		items += '<li><span class = "color" style = "background:' + legendColors[index].legend[i].color + '"></span>' + legendColors[index].legend[i].cName + '</li>';
	}*/
	
	legendUl.empty(); // Reset legend list
	legendUl.append(items);
}

function createTable()
{
	var table = $('#results table');

	var tString = "";

	for(var i = 0; i < userResp.length; i++)
	{
		tString += '<tr><td>' + userResp[i].answer + '</td></tr>' +
							 '<tr><td>' + userResp[i].time + '</td></tr>';
	}

	table.append(tString);
	$('#results').css('display','block');
	$('#mainTest').css('display','none');
}

