var start;
var stop;
var time;

var lastQuestion = false;

// Class userResp
var Response = function(ans, time, pts)
{
	this.answer = ans; // String.
	this.time = time;	// int.
	this.points = pts; // int
}

var indexExp = 0; // The index counter for experiment sample.

// Arrays

var userResp = [];

legendColors = 
[
	// R1
	{ name:"R1-org", type: "org", src: "img/samples/R1-org.png", correct: "2",   titleText : "Built-up Area"}, // R
	{ name:"R3-fid", type: "fid", src: "img/samples/R3-fid.png", correct: "0",   titleText : "Rockery" },	// R	
	{ name:"R4-chr", type: "chr", src: "img/samples/R4-chr.png", correct: "1,2", titleText : "Forest"},
	{ name:"R1-vis", type: "vis", src: "img/samples/R1-vis.png", correct: "3",   titleText : "Built-up Area"},
	
	// R2 	
	{ name:"R3-chr", type: "chr", src: "img/samples/R3-chr.png", correct: "2",   titleText : "Rockery" }, 
	{ name:"R4-org", type: "org", src: "img/samples/R4-org.png", correct: "0,3", titleText : "Forest"},		// R
	{ name:"R2-fid", type: "fid", src: "img/samples/R2-fid.png", correct: "0,2", titleText : "Woods"},		// R
	{ name:"R4-vis", type: "vis", src: "img/samples/R4-vis.png", correct: "1,2", titleText : "Forest"},

 	// R3
 	{ name:"R1-fid", type: "fid", src: "img/samples/R1-fid.png", correct: "2",   titleText : "Built-up Area"}, // R
	{ name:"R3-org", type: "org", src: "img/samples/R3-org.png", correct: "0",   titleText : "Rockery"},			// R
	{ name:"R3-vis", type: "vis", src: "img/samples/R3-vis.png", correct: "2",   titleText : "Rockery"},
 	{ name:"R2-chr", type: "chr", src: "img/samples/R2-chr.png", correct: "2,3", titleText : "Woods"},

	 // R4	 
	{ name:"R2-vis", type: "vis", src: "img/samples/R2-vis.png", correct: "0,2", titleText : "Woods"}, 	 	// R
 	{ name:"R4-fid", type: "fid", src: "img/samples/R4-fid.png", correct: "0,3", titleText : "Forest"},		// R
	{ name:"R1-chr", type: "chr", src: "img/samples/R1-chr.png", correct: "3",   titleText : "Built-up Area"},
	{ name:"R2-org", type: "org", src: "img/samples/R2-org.png", correct: "2,3", titleText : "Woods"}
  
];

colorAlg = 
[
	// 0: Original
	{
		alg: "org", 
	  legend:
		[
			{ color: "#f7be8c", cName: "Agglomeration" },
			{ color: "#ffdcbe", cName: "Built-up area" }, 
		 	{ color: "#d2e67c", cName: "Forest" }, 
		 	{ color: "#f2d999", cName: "Height level" }, 
		 	{ color: "#e0ffff", cName: "Ocean" }, 
		 	{ color: "#ffffe6", cName: "Open area" }, 
		 	{ color: "#b2b2b2", cName: "Road" }, 
		 	{ color: "#d7d7d7", cName: "Rockery" }, 
		 	{ color: "#00a6ff", cName: "River" }, 
		 	{ color: "#fff7a3", cName: "Woods" } 	
		]
	},

	// 1: Fidaner
	{
		alg: "fid", 
	  legend:
		[
			{ color: "#efdab0", cName: "Agglomeration" },
			{ color: "#faedd3", cName: "Built-up area" }, 
		 	{ color: "#d4db76", cName: "Forest" }, 
		 	{ color: "#eee5aa", cName: "Height level" }, 
		 	{ color: "#e4eeeb", cName: "Ocean" }, 
		 	{ color: "#ffffe5", cName: "Open area" }, 
		 	{ color: "#b2b2b1", cName: "Road" },
		 	{ color: "#d6d6d5", cName: "Rockery" },
		 	{ color: "#005198", cName: "River" }, 
		 	{ color: "#fefbaa", cName: "Woods" }
		]
	},

	// 2: Chrome
	{
		alg: "chr", 
	  legend:
		[
			{ color: "#e6b5fc", cName: "Agglomeration" },
			{ color: "#f4d6fd", cName: "Built-up area" }, 
		 	{ color: "#d8e74d", cName: "Forest" }, 
		 	{ color: "#ead4eb", cName: "Height level" }, 
		 	{ color: "#e9ff9b", cName: "Ocean" }, 
		 	{ color: "#ffffe5", cName: "Open area" },
		 	{ color: "#b2b2b1", cName: "Road" }, 
		 	{ color: "#d6d6d5", cName: "Rockery" }, 
		 	{ color: "#00bc00", cName: "River" },
		 	{ color: "#fcf5be", cName: "Woods" }	
		]
	},

	// 3: Visicheck
	{
		alg: "vis", 
	  legend:
		[
			{ color: "#fed0d3", cName: "Agglomeration" },
			{ color: "#ffeeff", cName: "Built-up area" }, 
		 	{ color: "#f6f4aa", cName: "Forest" }, 
		 	{ color: "#ffead9", cName: "Height level" }, 
		 	{ color: "#ffffff", cName: "Ocean" }, 
		 	/*{ color: "#ffffff", cName: "Open area" },*/ 
		 	{ color: "#d6cade", cName: "Road" },
		 	{ color: "#fbe4ff", cName: "Rockery" }, 
		 	{ color: "#008bce", cName: "River" },
		 	{ color: "#ffffe3", cName: "Woods" }	
		]
	}

];