<!doctype html>
<html lang= "en">
<head>
	<meta name="viewport" content="width = device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>Demo</title>
	
	<!-- Libraries -->
	<!-- jQuery 2.2.4 --><script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
	
	<!-- Style -->
	<link rel="stylesheet" href="css/global.css"/>
	
	<!-- Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Droid+Sans" rel="stylesheet">
	
	<!-- Javascript -->
	<script type = "text/javascript" src = "js/variables.js"></script>
	<script type = "text/javascript" src = "js/globalFunctions.js"></script>
	<script type = "text/javascript" src = "js/userInteraction.js"></script>

	
</head>

<body style="padding: 5%;">	

<div id="mainTest">

	<div class="question">
		Select <b>all</b> color patches that match: <span style="text-decoration: underline;">Apple</span>.
	</div>

	<div class="colorPatches">
		<input type="checkbox" data-slot="0">
		<input type="checkbox" data-slot="1">
			
		<img src="img/demo.png" id="sample">	

		<input type="checkbox" data-slot="2">
		<input type="checkbox" data-slot="3">
	</div>

	<div id="legend">
		<h2 class = "legendTitle">legend</h2>
		<ul>
			<li><span class = "color" style = "background: red;"></span>Apple</li>
			<li><span class = "color" style = "background: yellow;"></span>Banana</li>
			<li><span class = "color" style = "background: green;"></span>Grape</li>
			<li><span class = "color" style = "background: orange;"></span>Orange</li>
		</ul>
	</div>

	<div id="space">Press Space-bar to proceed</div>
	
	<button id="stop">Finish</button>


	<div id="overlay"></div>
	
	<div id="startScreen">
		<h2>Experiment</h2>
		<br><h3>Demo</h3>
		
		<button id="start">Start</button>
	</div>

</div>
	
</body>
</html>