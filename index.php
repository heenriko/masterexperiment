<!doctype html>
<html lang= "en">
<head>
	<meta name="viewport" content="width = device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>Color Experiment</title>
	
	<!-- Libraries -->
	<!-- jQuery 2.2.4 --><script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
	
	<!-- Style -->
	<link rel="stylesheet" href="css/global.css"/>
	
	<!-- Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400" rel="stylesheet">
	
	<!-- Javascript -->
	<script type = "text/javascript" src = "js/classes/Observer.js"></script>
	<script type = "text/javascript" src = "js/helpFn.js"></script>
	<script type = "text/javascript" src = "js/variables.js"></script>
	<script type = "text/javascript" src = "js/globalFunctions.js"></script>
	<script type = "text/javascript" src = "js/userInteraction.js"></script>
	
</head>

<body>	

<!-- Start screen -->
<div id="overlay"></div>
<div id="startScreen">
	<h2>experiment</h2>
	<p>
		This experiment contains 24 tasks – Your objective is to find and click on
		a specific road on the map that match the color displayed in the legend.
	</p>
	<label for="formAge">Enter your age</label>
	<input id="formAge" type="number" placeholder="age" required>

	<label for="formGender">Enter your gender</label>
	<select id="formGender" placeholder="Gender" required>
		<option value="0">Female</option>
		<option value="1">Male</option>
		<option value="2">Prefer not to say</option>
	</select><br>
	
	<label for="formColorVision">Color Vision (Ask Facilitator)</label>
	<select id="formColorVision" placeholder="Color vision" required>
		<option value="protanope">Protanope</option>
		<option value="protanomalous">Protanomalous</option>
		<option value="deuteranope">Deuteranope</option>
		<option value="deuteranomalous">Deuteranomalous</option>
		<option value="tritranope">tritranope</option>
		<option value="tritranomalous">tritranomalous</option>
		<option value="notCVD">Not CVD</option>
	</select>

	<button id="startTest">Start</button>

</div>

<!-- End start screen -->

<div id="objective">
		<h4>objective</h4>
		<p style="color: #ddd;">Select <span id="objectiveRoad" style="color: #ddd; font-weight: 400;"></span></p>
	</div>

<div id="masterContainer">
	<div id="mapContainer">
	<!-- <img src="img/bmap.png" id="basemap" /> -->
		<svg
			xmlns:svg="http://www.w3.org/2000/svg"
			xmlns="http://www.w3.org/2000/svg"
			xmlns:xlink="http://www.w3.org/1999/xlink"
			width="1800"
			height="1125"
			viewBox="0 0 1800 1125">

			<image
				width="1800"
				height="1125"
				preserveAspectRatio="none"
				xlink:href=""
				x="0"
				y="0" />
		</svg>
	</div> <!-- mapsContainer -->

	<div id="sidepanel">
		<div class="legend">
			<h2>LEGEND</h2>	
			<ul></ul>
		</div>
	</div>

</div>  <!-- End masterContainer -->


<div id="results">
	<h1>Thank you for participating<br>in this experiment!</h1>
	<table id="tableResults"></table>
	<!-- <div id="points"></div> -->
</div> 

</body>
</html>